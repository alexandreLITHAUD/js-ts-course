﻿/// <reference path="../patterns/observer.ts" />

/**
 * View responsible for the HUD
 */
class ViewHUD implements Observer
{
    //Controller of the application
    private _controller: Controller;

    /**
     * Constructor
     * @param controller Controller of the application
     */
    constructor(controller: Controller)
    {
        this._controller = controller;
        this._controller.addObserver(this);
    }

    /**
     * Notify the view
     */
    notify()
    {
        $('#zombie').text(this._controller.getScore());
    }
}