/// <reference path="../patterns/observer.ts" />
/**
 * View responsible for the drawing area
 */
var ViewDrawing = /** @class */ (function () {
    /**
     * Constructor
     * @param controller Controller of the application
     */
    function ViewDrawing(controller) {
        var _this = this;
        this._controller = controller;
        this._controller.addObserver(this);
        this._canvas = $('#drawing-area')[0];
        $('#drawing-area').on('mousemove', function (event) {
            _this._controller.pointTo(event.clientX, event.clientY);
        });
        $('#drawing-area').on('mousedown', function (event) {
            _this._controller.shoot();
        });
        $(window).on('resize', function () { _this.resize(); });
        this.resize();
        this.draw();
    }
    ViewDrawing.prototype.draw = function () {
        var _this = this;
        if (!this._controller.isLoading)
            this._controller.draw(this._canvas);
        window.requestAnimationFrame(function () { _this.draw(); });
    };
    /**
     * Notifies the view
     */
    ViewDrawing.prototype.notify = function () {
        // this._controller.draw(this._canvas);
    };
    /**
     * Resizes the canvas when the browser is resized
     */
    ViewDrawing.prototype.resize = function () {
        this._canvas.width = $('#drawing-area').width();
        this._canvas.height = $('#drawing-area').height();
        this._controller.resize(this._canvas.width, this._canvas.height);
    };
    return ViewDrawing;
}());
//# sourceMappingURL=view-drawing.js.map