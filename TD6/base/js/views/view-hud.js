/// <reference path="../patterns/observer.ts" />
/**
 * View responsible for the HUD
 */
var ViewHUD = /** @class */ (function () {
    /**
     * Constructor
     * @param controller Controller of the application
     */
    function ViewHUD(controller) {
        this._controller = controller;
        this._controller.addObserver(this);
    }
    /**
     * Notify the view
     */
    ViewHUD.prototype.notify = function () {
        $('#zombie').text(this._controller.getScore());
    };
    return ViewHUD;
}());
//# sourceMappingURL=view-hud.js.map