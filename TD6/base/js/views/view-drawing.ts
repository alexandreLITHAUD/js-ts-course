﻿/// <reference path="../patterns/observer.ts" />

/**
 * View responsible for the drawing area
 */
class ViewDrawing implements Observer
{
    //Controller of the application
    private _controller: Controller;

    //Canvas of the application
    private _canvas: HTMLCanvasElement;

    /**
     * Constructor
     * @param controller Controller of the application
     */
    constructor(controller: Controller)
    {
        this._controller = controller;
        this._controller.addObserver(this);

        this._canvas = <HTMLCanvasElement>$('#drawing-area')[0];

        $('#drawing-area').on('mousemove', (event) =>
        {
            this._controller.pointTo(event.clientX, event.clientY);
        });

        $('#drawing-area').on('mousedown', (event) =>
        {
            this._controller.shoot();
        });

        $(window).on('resize', () => { this.resize(); });

        this.resize();

        this.draw();
    }

    draw()
    {
        if (!this._controller.isLoading)
            this._controller.draw(this._canvas);

        window.requestAnimationFrame(() => { this.draw(); });
    }
    /**
     * Notifies the view
     */
    notify()
    {
        
           // this._controller.draw(this._canvas);
    }

    /**
     * Resizes the canvas when the browser is resized
     */
    resize()
    {
        this._canvas.width = $('#drawing-area').width();
        this._canvas.height = $('#drawing-area').height();
        
        this._controller.resize(this._canvas.width, this._canvas.height);

    }
}