﻿///<reference path="observer.ts" />

/**
 * Classe Sujet (Pattern Sujet / Observateur)
 */
class Subject
{
	private observers: Array<Observer>;

	/**
	 * Constructor
	 */
	constructor()
	{
        this.observers = new Array<Observer>();
	}

	/**
	 * Add an observer to the subject
	 * @param observer {Observer} Observer to add to the subject
	 */
	addObserver(observer: Observer)
	{
		this.observers.push(observer);
	}

	/**
	 * Notifies all observers of the subject
	 * @param notification {Notification} Informations transmises aux observateurs lors de la notification
	 */
    notify()
	{
		this.observers.forEach((observer: Observer) => { observer.notify(); });
	}
}