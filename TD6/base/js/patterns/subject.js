///<reference path="observer.ts" />
/**
 * Classe Sujet (Pattern Sujet / Observateur)
 */
var Subject = /** @class */ (function () {
    /**
     * Constructor
     */
    function Subject() {
        this.observers = new Array();
    }
    /**
     * Add an observer to the subject
     * @param observer {Observer} Observer to add to the subject
     */
    Subject.prototype.addObserver = function (observer) {
        this.observers.push(observer);
    };
    /**
     * Notifies all observers of the subject
     * @param notification {Notification} Informations transmises aux observateurs lors de la notification
     */
    Subject.prototype.notify = function () {
        this.observers.forEach(function (observer) { observer.notify(); });
    };
    return Subject;
}());
//# sourceMappingURL=subject.js.map