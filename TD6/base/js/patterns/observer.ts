﻿/**
 * Observer
 */
interface Observer
{
	/**
	 * Notify function
	 */
    notify();
}