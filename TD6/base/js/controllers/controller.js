/// <reference path="../patterns/subject.ts" />
/// <reference path="../models/textures-manager.ts" />
/// <reference path="../models/game.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Controller responsible for the application
 */
var Controller = /** @class */ (function (_super) {
    __extends(Controller, _super);
    /**
     * Constructor
     */
    function Controller() {
        var _this = _super.call(this) || this;
        _this._game = new Game();
        return _this;
    }
    Object.defineProperty(Controller.prototype, "game", {
        get: function () { return this._game; },
        set: function (value) { this._game = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Controller.prototype, "isLoading", {
        get: function () { return this._isLoading; },
        enumerable: false,
        configurable: true
    });
    ;
    /**
     * Initialise the application
     */
    Controller.prototype.initialise = function () {
        var _this = this;
        this._isLoading = true;
        this.notify();
        var texturesManager = TexturesManager.getInstance().loadTextures().then(function () {
            _this._isLoading = false;
            _this._game.initialize();
            _this.start();
            _this.notify();
        });
    };
    /**
     * Start the game
     */
    Controller.prototype.start = function () {
        var _this = this;
        this._timer = setInterval(function () {
            _this._game.iterate();
            if (_this._game.gameOver)
                clearInterval(_this._timer);
            _this.notify();
        }, 100);
    };
    /**
     * resize the game ground
     * @param width Width of the ground
     * @param height Height of the ground
     */
    Controller.prototype.resize = function (width, height) {
        this.game.resize(width, height);
        this.notify();
    };
    /**
     * Angle the player to the given coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    Controller.prototype.pointTo = function (x, y) {
        this.game.pointTo(x, y);
    };
    Controller.prototype.shoot = function () {
        this.game.shoot();
    };
    /**
     * Draw the game on the given canvas
     * @param canvas Canvas to use for drawing
     */
    Controller.prototype.draw = function (canvas) {
        if (!this.isLoading)
            this.game.draw(canvas);
    };
    /**
     * Return the number of humans on the game
     */
    Controller.prototype.getScore = function () {
        return this.game.score;
    };
    return Controller;
}(Subject));
//# sourceMappingURL=controller.js.map