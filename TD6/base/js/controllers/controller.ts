﻿/// <reference path="../patterns/subject.ts" />
/// <reference path="../models/textures-manager.ts" />
/// <reference path="../models/game.ts" />

/**
 * Controller responsible for the application
 */
class Controller extends Subject
{
    /**
     * Game manipulated by the controller
     */
    private _game: Game;
    public get game(): Game{ return this._game; };
    public set game(value: Game) { this._game = value; };

    /**
     * Indicates whether the controller is loading something
     */
    private _isLoading: boolean;
    public get isLoading(): boolean{ return this._isLoading; };

    /**
     * Timer which is responsible for the refreshing of the game
     */
    private _timer: number;


    /**
     * Constructor
     */
    constructor()
    {
        super();

        this._game = new Game();
    }

    /**
     * Initialise the application
     */
    initialise()
    {
        this._isLoading = true;
        this.notify();

        let texturesManager = TexturesManager.getInstance().loadTextures().then(() =>
        {
            this._isLoading = false;

            this._game.initialize();
            this.start();
            this.notify();
        });
    }

    /**
     * Start the game
     */
    start()
    {
        this._timer = setInterval(() =>
        {
            this._game.iterate();

            if (this._game.gameOver)
                clearInterval(this._timer);

            this.notify()
        }, 100);
    }

    /**
     * resize the game ground
     * @param width Width of the ground
     * @param height Height of the ground
     */
    resize(width: number, height: number)
    {
        this.game.resize(width, height);

        this.notify();
    }

    /**
     * Angle the player to the given coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    pointTo(x, y)
    {
        this.game.pointTo(x, y);
    }

    shoot()
    {
        this.game.shoot();
    }

    /**
     * Draw the game on the given canvas
     * @param canvas Canvas to use for drawing
     */
    draw(canvas: HTMLCanvasElement)
    {
        if (!this.isLoading)
            this.game.draw(canvas);
    }

    /**
     * Return the number of humans on the game
     */
    getScore()
    {
        return this.game.score;
    }
}