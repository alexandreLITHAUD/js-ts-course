/// <reference path="../../definitely-typed/jquery.d.ts" />
/**
 * Zombies Attack Application
 */
var Application = /** @class */ (function () {
    /**
     * Constructor
     */
    function Application() {
        this._controller = new Controller();
        this._viewHUD = new ViewHUD(this._controller);
        this._viewDrawing = new ViewDrawing(this._controller);
        this._controller.initialise();
    }
    return Application;
}());
/**
 * Start the application when the page is loaded
 */
var zombiesDefense;
$(window).ready(function () {
    zombiesDefense = new Application();
});
//# sourceMappingURL=application.js.map