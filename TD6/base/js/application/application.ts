﻿/// <reference path="../../definitely-typed/jquery.d.ts" />

/**
 * Zombies Attack Application
 */
class Application
{
    private _controller: Controller;
    private _viewHUD: ViewHUD;
    private _viewDrawing: ViewDrawing;

    /**
     * Constructor
     */
    constructor()
    {
        this._controller = new Controller();
        this._viewHUD = new ViewHUD(this._controller);
        this._viewDrawing = new ViewDrawing(this._controller);

        this._controller.initialise();
    }
}


/**
 * Start the application when the page is loaded
 */
let zombiesDefense: Application;

$(window).ready(() =>
{
    zombiesDefense = new Application();
});