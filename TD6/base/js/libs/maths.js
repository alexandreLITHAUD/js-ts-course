var Maths = /** @class */ (function () {
    function Maths() {
    }
    /**
     * return the distance between two points
     * @param x1 : x coordinate of the first point
     * @param y1 : y coordinate of the first point
     * @param x2 : x coordinate of the second point
     * @param y2 : y coordinate of the second point
     */
    Maths.getDistance = function (x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };
    /**
     * return the angle between the x axis and the line passing through the given points
     * @param x1 : x coordinate of the first point
     * @param y1 : y coordinate of the first point
     * @param x2 : x coordinate of the second point
     * @param y2 : y coordinate of the second point
     */
    Maths.getAngle = function (x1, y1, x2, y2) {
        var radius = Maths.getDistance(x1, y1, x2, y2);
        var angle = Math.asin(Math.abs(y2 - y1) / radius);
        if (x2 < x1)
            angle = Math.PI - angle;
        if (y2 < y1)
            angle = 2 * Math.PI - angle;
        return angle;
    };
    return Maths;
}());
//# sourceMappingURL=maths.js.map