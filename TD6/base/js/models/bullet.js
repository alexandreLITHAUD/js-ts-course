var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Bullet = /** @class */ (function (_super) {
    __extends(Bullet, _super);
    function Bullet(x, y, direction) {
        return _super.call(this, x, y, 0, 5, 15, direction) || this;
    }
    Bullet.prototype.draw = function (canvas) {
        var ctx = canvas.getContext("2d");
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.direction);
        ctx.rect(0, 0, 3, 5);
        ctx.stroke();
        ctx.restore();
    };
    return Bullet;
}(MovableItem));
//# sourceMappingURL=bullet.js.map