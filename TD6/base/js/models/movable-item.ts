﻿/// <reference path="drawable-item.ts" />

/**
 * Represent a movable item
 */
class MovableItem extends DrawableItem
{
    /**
     * Moving speed of the item
     */
    private _speed: number;
    public get speed(): number{ return this._speed; };
	public set speed(value: number) { this._speed = value; };

    /**
     * Direction of the item (angle in radian)
     */
    private _direction: number;
    public get direction(): number{ return this._direction; };
    public set direction(value: number) { this._direction = value; this.rotation = value; };

    /**
     * Constructor
     * @param x original x coordinate
     * @param y original y coordinate
     * @param rotation original angle of rotation of the item
     * @param speed original speed of the item
     * @param direction original direction of the item
     */
    constructor(x: number = 0, y: number = 0, rotation: number = 0, size: number = 0, speed: number = 0, direction: number = 0)
    {
        super(x, y, direction, size);

        this._speed = speed;
        this._direction = direction;
    }

    /**
     * Move the item according to its speed and direction
     */
    move()
    {
        this.x += Math.cos(this.direction) * this.speed;
        this.y += Math.sin(this.direction) * this.speed;
    }
}