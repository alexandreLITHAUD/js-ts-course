﻿class Ground
{
    /**
     * Ground texture
     */
    private _texture: HTMLImageElement;
    public get texture(): HTMLImageElement { return this._texture; };
    public set texture(value: HTMLImageElement) { this._texture = value; };

    /**
     * Ground width
     */
    private _width: number;
    public get width(): number { return this._width; };
    public set width(value: number) { this._width = value; };

    /**
     * Ground height
     */
    private _height: number;
    public get height(): number { return this._height; };
    public set height(value: number) { this._height = value; };

    /**
     * Constructor
     * @param width Ground width
     * @param height Ground height
     */
    constructor(width: number = 0, height: number = 0)
    {
        this._width = width;
        this.height = height;
        this._texture = null;
    }

    initialize()
    {
        this._texture = TexturesManager.getInstance().getTexture('ground');
    }

    /**
     * Draw the ground of the game
     * @param canvas Canvas to use to draw the ground
     */
    draw(canvas: HTMLCanvasElement)
    {
        if (this._texture)
        {
            let context = canvas.getContext('2d');

            let coefW = this.width / this.texture.width;
            let coefH = this.height / this.texture.height;

            let coef = Math.max(coefW, coefH);

            if (coef > 1)
                context.drawImage(this.texture, -(this.width - this.texture.width * coef) / 2, -(this.height - this.texture.height * coef) / 2, this.width, this.height);
            else
                context.drawImage(this.texture, -this.texture.width / 2, -this.texture.height / 2);
        }
    }
}