/// <reference path="collection.ts" />
/// <reference path="../libs/maths.ts" />
/**
 * Represent a drawable item
 */
var DrawableItem = /** @class */ (function () {
    /**
     * Constructor
     * @param x original x coordinate
     * @param y original y coordinate
     * @param rotation original angle of rotation of the item
     */
    function DrawableItem(x, y, rotation, size) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (rotation === void 0) { rotation = 0; }
        if (size === void 0) { size = 0; }
        this._x = x;
        this._y = y;
        this._rotation = rotation;
        this._size = size;
    }
    Object.defineProperty(DrawableItem.prototype, "x", {
        get: function () { return this._x; },
        set: function (value) { this._x = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(DrawableItem.prototype, "y", {
        get: function () { return this._y; },
        set: function (value) { this._y = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(DrawableItem.prototype, "rotation", {
        get: function () { return this._rotation; },
        set: function (value) { this._rotation = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(DrawableItem.prototype, "size", {
        get: function () { return this._size; },
        set: function (value) { this._size = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    /**
     * Draw the item on the given canvas
     */
    DrawableItem.prototype.draw = function (canvas) {
    };
    /**
     * Test whether this item is touching the given item
     * @param item Item with which to test
     */
    DrawableItem.prototype.isTouching = function (item) {
        var touching = false;
        var distance = Maths.getDistance(this.x, this.y, item.x, item.y);
        if (distance <= this.size + item.size)
            touching = true;
        return touching;
    };
    return DrawableItem;
}());
//# sourceMappingURL=drawable-item.js.map