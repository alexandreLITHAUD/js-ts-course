var Map = /** @class */ (function () {
    /**
     * Constructor
     */
    function Map() {
        this._items = {};
    }
    /**
     * Set a value of the map
     * @param key Unique key related to the value
     * @param value value related to the key
     */
    Map.prototype.setValue = function (key, value) { this._items[key] = value; };
    ;
    /**
     * Return the value related to the given key
     * @param key Unique key related to the searched value
     */
    Map.prototype.getValue = function (key) {
        return this._items[key];
    };
    ;
    return Map;
}());
//# sourceMappingURL=map.js.map