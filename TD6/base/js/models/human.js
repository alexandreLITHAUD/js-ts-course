var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Represent a human
 */
var Human = /** @class */ (function (_super) {
    __extends(Human, _super);
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     */
    function Human(x, y, direction) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (direction === void 0) { direction = 0; }
        var _this = _super.call(this, x, y, 0, 32) || this;
        _this._ammo = 0;
        _this._kills = 0;
        _this._shield = false;
        _this._timerShield = null;
        return _this;
    }
    Object.defineProperty(Human.prototype, "shield", {
        get: function () { return this._shield; },
        enumerable: false,
        configurable: true
    });
    ;
    Object.defineProperty(Human.prototype, "ammo", {
        get: function () { return this._ammo; },
        set: function (value) { this._ammo = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Human.prototype, "kills", {
        get: function () { return this._kills; },
        set: function (value) { this._kills = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Human.prototype.getGunCoordinates = function () {
        return new GunCoordinates(this.x + 33 * Math.cos(this.rotation) - 15 * Math.sin(this.rotation), this.y + 33 * Math.sin(this.rotation) + 15 * Math.cos(this.rotation));
    };
    /**
     * Active the shield
     */
    Human.prototype.activeShield = function () {
        var _this = this;
        clearTimeout(this._timerShield);
        this._shield = true;
        this._timerShield = setTimeout(function () { _this.disableShield(); }, 5000);
    };
    /**
     * Disable the shield
     */
    Human.prototype.disableShield = function () {
        this._shield = false;
    };
    Human.prototype.draw = function (canvas) {
        var ctx = canvas.getContext("2d");
        ctx.save();
        ctx.rotate(this.rotation);
        ctx.beginPath();
        ctx.strokeStyle = "#000000";
        var radial = ctx.createRadialGradient(0, 0, 0, 0, 0, 40);
        radial.addColorStop(0, "#6E717E");
        radial.addColorStop(1, "#13311A");
        ctx.fillStyle = radial;
        ctx.arc(0, 0, 10, 0, 2 * Math.PI);
        ctx.ellipse(0, 0, 8, 20, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "#000000";
        var gradient = ctx.createLinearGradient(0, 0, 25, 20);
        gradient.addColorStop(0, "#777");
        gradient.addColorStop(1, "#333");
        ctx.fillStyle = gradient;
        ctx.rect(5, 17, 15, 4);
        ctx.rect(20, 17, 4, 2);
        ctx.rect(24, 17, 4, 3);
        ctx.fill();
        ctx.stroke();
        ctx.restore();
    };
    return Human;
}(DrawableItem));
//# sourceMappingURL=human.js.map