﻿class Map<T>
{
    /**
     * Items of the map
     */
    private _items: any;

    /**
     * Constructor
     */
    constructor()
    {
        this._items = {};
    }

    /**
     * Set a value of the map
     * @param key Unique key related to the value
     * @param value value related to the key
     */
    setValue(key: string, value: T) { this._items[key] = value;  };

    /**
     * Return the value related to the given key
     * @param key Unique key related to the searched value
     */
    getValue(key: string): T
    {
        return this._items[key];
    };
}