﻿class Bullet extends MovableItem
{
    constructor(x: number, y: number, direction: number)
    {
        super(x, y, 0, 5, 15, direction);
    }

    draw(canvas: HTMLCanvasElement) {
        let ctx = canvas.getContext("2d");

        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.direction);    
        ctx.rect(0, 0, 3, 5);
        ctx.stroke();
        ctx.restore();
    }

    
}