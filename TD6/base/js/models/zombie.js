var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * represent a Zombie
 */
var Zombie = /** @class */ (function (_super) {
    __extends(Zombie, _super);
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     * @param direction Original direction of the zombie
     */
    function Zombie(x, y, direction) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (direction === void 0) { direction = 0; }
        var _this = _super.call(this, x, y, 0, 32, 3, direction) || this;
        _this._timerSpeed = null;
        return _this;
    }
    /**
     * Boost the speed of the zombie
     */
    Zombie.prototype.boostSpeed = function () {
        var _this = this;
        clearTimeout(this._timerSpeed);
        this.speed = 10;
        this._timerSpeed = setTimeout(function () { _this.speed = 3; }, 3000);
    };
    Zombie.prototype.draw = function (canvas) {
        var ctx = canvas.getContext("2d");
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.direction);
        var image = TexturesManager.getInstance().getTexture(this.getTextureName());
        ctx.drawImage(image, 0, 0);
        ctx.restore();
    };
    return Zombie;
}(MovableItem));
//# sourceMappingURL=zombie.js.map