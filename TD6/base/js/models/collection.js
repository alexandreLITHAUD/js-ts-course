/**
 * Collection of objects
 */
var Collection = /** @class */ (function () {
    /**
     * Constructor
     */
    function Collection() {
        this.elements = new Array();
    }
    /**
     * Clear the collection
     */
    Collection.prototype.clear = function () {
        this.elements = new Array();
    };
    Object.defineProperty(Collection.prototype, "length", {
        /**
         * Return the number of items in the collection
         * @return {number} Size of the collection
         */
        get: function () {
            return this.elements.length;
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Add an element to the collection
     * @param {T} element Element ot add to the collection
     */
    Collection.prototype.add = function (element) {
        this.elements.push(element);
    };
    /**
     * Return an element from the collection
     * @param {number} index Index of the item to return
     * @throw Throw an exception if the index is incorrect
     */
    Collection.prototype.at = function (index) {
        if (index < 0 || index >= this.length)
            throw 'Collection::at - Index out of range (' + index + ', 0 - ' + this.length + ').';
        return this.elements[index];
    };
    /**
     * REmove an item from the collection
     * @param {number} index Index of the item to remove
     * @throw Throw an exception if the index is incorrect
     */
    Collection.prototype.remove = function (index) {
        if (index < 0 || index >= this.length)
            throw 'Collection::remove - Index out of range (' + index + ', 0 - ' + this.length + ').';
        this.elements.splice(index, 1);
    };
    Collection.prototype.forEach = function (callback) {
        for (var i = 0; i < this.length; ++i)
            callback(this.at(i));
    };
    return Collection;
}());
//# sourceMappingURL=collection.js.map