﻿/// <reference path="../../definitely-typed/es6-promise.d.ts" />
/// <reference path="map.ts" />

/**
 * Loads textures
 */
class TexturesManager
{
    /**
     * Public instance of the singleton
     */
    public static _instance: TexturesManager = null;

    private _textures: Map<HTMLImageElement>;

    /**
     * Constructor
     */
    private constructor()
    {
        this._textures = new Map<HTMLImageElement>();
    }

    /**
     * Return the instance of the singleton
     */
    public static getInstance(): TexturesManager
    {
        if (TexturesManager._instance === null)
            TexturesManager._instance = new TexturesManager();
                
        return TexturesManager._instance;
    }

    /**
     * Load a picture
     * @param path Path to the picture to load
     */
    private load(textureName: string, path: string): Promise<void>
    {
        return new Promise((resolve, reject) =>
        {
            let image = new Image();
            image.onload = () =>
            {
                this._textures.setValue(textureName, image);

                resolve();
            };          
            
            image.src = path;
        });
    }

    /**
     * Load all the needed textures for the game
     */
    public loadTextures(): Promise<void>
    {
        return new Promise((resolve, reject) =>
        {
            let promises = new Array<Promise<any>>();

            promises.push(this.load('ground', 'img/textures/ground.jpg'));

            promises.push(this.load('bullets', 'img/textures/bullets.png'));
            promises.push(this.load('dead', 'img/textures/dead.png'));

            promises.push(this.load('zombie11', 'img/textures/zombie-1-1.png'));
            promises.push(this.load('zombie12', 'img/textures/zombie-1-2.png'));
            promises.push(this.load('zombie13', 'img/textures/zombie-1-3.png'));
            promises.push(this.load('zombie14', 'img/textures/zombie-1-4.png'));
            promises.push(this.load('zombie15', 'img/textures/zombie-1-5.png'));
            promises.push(this.load('zombie16', 'img/textures/zombie-1-6.png'));
            promises.push(this.load('zombie17', 'img/textures/zombie-1-7.png'));
            promises.push(this.load('zombie18', 'img/textures/zombie-1-8.png'));

            promises.push(this.load('zombie21', 'img/textures/zombie-2-1.png'));
            promises.push(this.load('zombie22', 'img/textures/zombie-2-2.png'));
            promises.push(this.load('zombie23', 'img/textures/zombie-2-3.png'));
            promises.push(this.load('zombie24', 'img/textures/zombie-2-4.png'));
            promises.push(this.load('zombie25', 'img/textures/zombie-2-5.png'));
            promises.push(this.load('zombie26', 'img/textures/zombie-2-6.png'));
            promises.push(this.load('zombie27', 'img/textures/zombie-2-7.png'));
            promises.push(this.load('zombie28', 'img/textures/zombie-2-8.png'));

            promises.push(this.load('human11', 'img/textures/human-1-1.png'));
            promises.push(this.load('human12', 'img/textures/human-1-2.png'));
            promises.push(this.load('human13', 'img/textures/human-1-3.png'));
            promises.push(this.load('human14', 'img/textures/human-1-4.png'));
            promises.push(this.load('human15', 'img/textures/human-1-5.png'));
            promises.push(this.load('human16', 'img/textures/human-1-6.png'));
            promises.push(this.load('human17', 'img/textures/human-1-7.png'));
            promises.push(this.load('human18', 'img/textures/human-1-8.png'));

            promises.push(this.load('ammo', 'img/textures/ammo.png'));
            promises.push(this.load('shield', 'img/textures/shield.png'));
            promises.push(this.load('glue', 'img/textures/glue.png'));
            promises.push(this.load('speed', 'img/textures/speed.png'));

            Promise.all(promises).then(() => { resolve() }).catch(() => { reject() });
        });
    }

    /**
     * Return the texture which has the given name
     * @param textureName Name of the texture to return
     */
    public getTexture(textureName: string): HTMLImageElement
    {
        let texture = this._textures.getValue(textureName);

        return texture;
    }
}