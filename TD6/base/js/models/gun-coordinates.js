var GunCoordinates = /** @class */ (function () {
    function GunCoordinates(x, y) {
        this._x = x;
        this._y = y;
    }
    Object.defineProperty(GunCoordinates.prototype, "x", {
        get: function () { return this._x; },
        set: function (value) { this._x = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(GunCoordinates.prototype, "y", {
        get: function () { return this._y; },
        set: function (value) { this._y = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    return GunCoordinates;
}());
//# sourceMappingURL=gun-coordinates.js.map