/// <reference path="textures-manager.ts" />
/// <reference path="zombie.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Represent a Scientist Zombie
 */
var ZombieScientist = /** @class */ (function (_super) {
    __extends(ZombieScientist, _super);
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     * @param direction Original direction of the zombie
     */
    function ZombieScientist(x, y, direction) {
        return _super.call(this, x, y, direction) || this;
    }
    ZombieScientist.prototype.getTextureName = function () {
        return "zombie11";
    };
    return ZombieScientist;
}(Zombie));
//# sourceMappingURL=zombie-scientist.js.map