﻿/// <reference path="collection.ts" />
/// <reference path="../libs/maths.ts" />

/**
 * Represent a drawable item
 */
class DrawableItem
{
    /**
     * X coordinate of the item
     */
    private _x: number;
    public get x(): number { return this._x; };
    public set x(value: number) { this._x = value; };

    /**
     * Y coordinate of the item
     */
    private _y: number;
    public get y(): number { return this._y; };
    public set y(value: number) { this._y = value; };

    /**
     * Angle of rotation (radians) of the item
     */
    private _rotation: number;
    public get rotation(): number { return this._rotation; };
    public set rotation(value: number) { this._rotation = value; };

    /**
     * Box sizing of the item
     */
    private _size: number;
    public get size(): number { return this._size; };
    public set size(value: number) { this._size = value; };

    /**
     * Constructor
     * @param x original x coordinate
     * @param y original y coordinate
     * @param rotation original angle of rotation of the item
     */
    constructor(x: number = 0, y: number = 0, rotation: number = 0, size: number = 0)
    {
        this._x = x;
        this._y = y;
        this._rotation = rotation;
        this._size = size;
    }

    /**
     * Draw the item on the given canvas
     */
    draw(canvas: HTMLCanvasElement)
    {
    }

    /**
     * Test whether this item is touching the given item
     * @param item Item with which to test
     */
    isTouching(item: DrawableItem): boolean
    {
        let touching = false;

        let distance = Maths.getDistance(this.x, this.y, item.x, item.y);

        if (distance <= this.size + item.size)
            touching = true;

        return touching;
    }
}