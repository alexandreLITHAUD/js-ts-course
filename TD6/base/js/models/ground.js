var Ground = /** @class */ (function () {
    /**
     * Constructor
     * @param width Ground width
     * @param height Ground height
     */
    function Ground(width, height) {
        if (width === void 0) { width = 0; }
        if (height === void 0) { height = 0; }
        this._width = width;
        this.height = height;
        this._texture = null;
    }
    Object.defineProperty(Ground.prototype, "texture", {
        get: function () { return this._texture; },
        set: function (value) { this._texture = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Ground.prototype, "width", {
        get: function () { return this._width; },
        set: function (value) { this._width = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Ground.prototype, "height", {
        get: function () { return this._height; },
        set: function (value) { this._height = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Ground.prototype.initialize = function () {
        this._texture = TexturesManager.getInstance().getTexture('ground');
    };
    /**
     * Draw the ground of the game
     * @param canvas Canvas to use to draw the ground
     */
    Ground.prototype.draw = function (canvas) {
        if (this._texture) {
            var context = canvas.getContext('2d');
            var coefW = this.width / this.texture.width;
            var coefH = this.height / this.texture.height;
            var coef = Math.max(coefW, coefH);
            if (coef > 1)
                context.drawImage(this.texture, -(this.width - this.texture.width * coef) / 2, -(this.height - this.texture.height * coef) / 2, this.width, this.height);
            else
                context.drawImage(this.texture, -this.texture.width / 2, -this.texture.height / 2);
        }
    };
    return Ground;
}());
//# sourceMappingURL=ground.js.map