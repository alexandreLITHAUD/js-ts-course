﻿/**
 * Collection of objects
 */
class Collection<T>
{
	//Collection of elements
	private elements: Array<T>;

	/**
	 * Constructor
	 */
	constructor()
	{
		this.elements = new Array<T>();
	}

	/**
	 * Clear the collection
	 */
	clear()
	{
		this.elements = new Array<T>();
	}

	/**
	 * Return the number of items in the collection
	 * @return {number} Size of the collection
	 */
	get length(): number
	{
		return this.elements.length;
	}

	/**
	 * Add an element to the collection
	 * @param {T} element Element ot add to the collection
	 */
	add(element: T)
	{
		this.elements.push(element);
	}

	/**
	 * Return an element from the collection
	 * @param {number} index Index of the item to return
	 * @throw Throw an exception if the index is incorrect
	 */
	at(index: number): T
	{
		if (index < 0 || index >= this.length)
			throw 'Collection::at - Index out of range (' + index + ', 0 - ' + this.length + ').';

		return this.elements[index];
	}

	/**
	 * REmove an item from the collection
	 * @param {number} index Index of the item to remove
	 * @throw Throw an exception if the index is incorrect
	 */
	remove(index: number)
	{
		if (index < 0 || index >= this.length)
			throw 'Collection::remove - Index out of range (' + index + ', 0 - ' + this.length + ').';

		this.elements.splice(index, 1);
    }

    forEach(callback: any)
    {
        for (let i = 0; i < this.length; ++i)
            callback(this.at(i));
    }
}