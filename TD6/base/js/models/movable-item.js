/// <reference path="drawable-item.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Represent a movable item
 */
var MovableItem = /** @class */ (function (_super) {
    __extends(MovableItem, _super);
    /**
     * Constructor
     * @param x original x coordinate
     * @param y original y coordinate
     * @param rotation original angle of rotation of the item
     * @param speed original speed of the item
     * @param direction original direction of the item
     */
    function MovableItem(x, y, rotation, size, speed, direction) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (rotation === void 0) { rotation = 0; }
        if (size === void 0) { size = 0; }
        if (speed === void 0) { speed = 0; }
        if (direction === void 0) { direction = 0; }
        var _this = _super.call(this, x, y, direction, size) || this;
        _this._speed = speed;
        _this._direction = direction;
        return _this;
    }
    Object.defineProperty(MovableItem.prototype, "speed", {
        get: function () { return this._speed; },
        set: function (value) { this._speed = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(MovableItem.prototype, "direction", {
        get: function () { return this._direction; },
        set: function (value) { this._direction = value; this.rotation = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    /**
     * Move the item according to its speed and direction
     */
    MovableItem.prototype.move = function () {
        this.x += Math.cos(this.direction) * this.speed;
        this.y += Math.sin(this.direction) * this.speed;
    };
    return MovableItem;
}(DrawableItem));
//# sourceMappingURL=movable-item.js.map