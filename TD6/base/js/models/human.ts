﻿/**
 * Represent a human
 */
class Human extends DrawableItem
{
    /**
     * Has the human a shield ?
     */
    private _shield: boolean;
    public get shield(): boolean { return this._shield; };

    /**
     * Timer responsible for the activation of the shield
     */
    private _timerShield: number;

    /**
     * How many ammo has the human ?
     */
    private _ammo: number;
    public get ammo(): number{ return this._ammo; };
    public set ammo(value: number) { this._ammo = value; };

    /**
     * How many zombies the human has killed ?
     */
    private _kills: number;
    public get kills(): number{ return this._kills; };
    public set kills(value: number) { this._kills = value; };
    
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     */
    constructor(x: number = 0, y: number = 0, direction: number = 0)
    {
        super(x, y, 0, 32);

        this._ammo = 0;
        this._kills = 0;
        this._shield = false;
        this._timerShield = null;
    }

    getGunCoordinates(): GunCoordinates
    {
        return new GunCoordinates(
            this.x + 33 * Math.cos(this.rotation) - 15 * Math.sin(this.rotation),
            this.y + 33 * Math.sin(this.rotation) + 15 * Math.cos(this.rotation)
        );
    }

    /**
     * Active the shield
     */
    activeShield()
    {
        clearTimeout(this._timerShield);

        this._shield = true;
        this._timerShield = setTimeout(() => { this.disableShield(); }, 5000);
    }

    /**
     * Disable the shield
     */
    disableShield()
    {
        this._shield = false;
    }

    draw(canvas: HTMLCanvasElement) {
        let ctx = canvas.getContext("2d");

        ctx.save();
        ctx.rotate(this.rotation);

        ctx.beginPath();
        ctx.strokeStyle = "#000000";
        let radial = ctx.createRadialGradient(0, 0, 0, 0, 0, 40);
        radial.addColorStop(0, "#6E717E");
        radial.addColorStop(1, "#13311A");
        ctx.fillStyle = radial;
        ctx.arc(0, 0, 10, 0, 2 * Math.PI);
        ctx.ellipse(0, 0, 8, 20, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = "#000000";
        let gradient = ctx.createLinearGradient(0, 0, 25, 20);
        gradient.addColorStop(0, "#777");
        gradient.addColorStop(1, "#333");
        ctx.fillStyle = gradient;
        ctx.rect(5, 17, 15, 4);
        ctx.rect(20, 17, 4, 2);
        ctx.rect(24, 17, 4, 3);
        ctx.fill();
        ctx.stroke();

        ctx.restore();
    }

    
}