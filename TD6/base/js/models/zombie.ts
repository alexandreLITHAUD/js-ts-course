﻿/**
 * represent a Zombie
 */
abstract class Zombie extends MovableItem
{
    /**
     * Timer responsible for speed booster
     */
    private _timerSpeed: number;

    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     * @param direction Original direction of the zombie
     */
    constructor(x: number = 0, y: number = 0, direction: number = 0)
    {
        super(x, y, 0, 32, 3, direction);

        this._timerSpeed = null;
    }

    /**
     * Boost the speed of the zombie
     */
    boostSpeed()
    {
        clearTimeout(this._timerSpeed);

        this.speed = 10;

        this._timerSpeed = setTimeout(() => { this.speed = 3; }, 3000);
    }

    protected abstract getTextureName();

    draw(canvas: HTMLCanvasElement) {
        let ctx = canvas.getContext("2d");
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.direction);
        let image = TexturesManager.getInstance().getTexture(this.getTextureName());
        ctx.drawImage(image, 0, 0);

        ctx.restore();
    }

}