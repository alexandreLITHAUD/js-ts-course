/// <reference path="collection.ts" />
/// <reference path="drawable-item.ts" />
/// <reference path="movable-item.ts" />
/// <reference path="ground.ts" />
/// <reference path="human.ts" />
/// <reference path="zombie-child.ts" />
/// <reference path="zombie-scientist.ts" />
/// <reference path="bullet.ts" />
/**
 * Represents the game
 */
var Game = /** @class */ (function () {
    /**
     * Constructor
     */
    function Game() {
        this._items = new Collection();
        this._ground = new Ground();
        this._player = new Human(0, 0, Math.PI / 2);
        this._gameOver = false;
        this._score = 0;
    }
    Object.defineProperty(Game.prototype, "score", {
        get: function () { return this._score; },
        set: function (value) { this._score = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Game.prototype, "gameOver", {
        get: function () { return this._gameOver; },
        set: function (value) { this._gameOver = value; },
        enumerable: false,
        configurable: true
    });
    ;
    ;
    /**
     * Initialize the game
     */
    Game.prototype.initialize = function () {
        this._ground.initialize();
        this._items.add(this._player);
        for (var i = 0; i < 5; ++i)
            this.createZombie();
        this.startTimerZombies();
    };
    /**
     * Adds a zombie on the ground
     */
    Game.prototype.createZombie = function () {
        var angle = Math.random() * Math.PI * 2;
        var x = (this._ground.width / 2 + 100) * Math.cos(angle);
        var y = (this._ground.width / 2 + 100) * Math.sin(angle);
        if (Math.random() < 0.5)
            this._items.add(new ZombieScientist(x, y, 0));
        else
            this._items.add(new ZombieChild(x, y, 0));
    };
    /**
     * Starts timer responsible for adding new zombies on the ground
     */
    Game.prototype.startTimerZombies = function () {
        var _this = this;
        this.stopTimerZombies();
        if (!this.gameOver) {
            this._timerZombies = setTimeout(function () {
                var zombiesNumber = Math.floor(Math.random() * 5 + 1);
                for (var i = 0; i < zombiesNumber; ++i)
                    _this.createZombie();
                _this.startTimerZombies();
            }, Math.random() * 1000 + 1000);
        }
    };
    /**
     * Stops timer responsible for adding zombies on the ground
     */
    Game.prototype.stopTimerZombies = function () {
        clearTimeout(this._timerZombies);
        this._timerZombies = null;
    };
    /**
     * Angle the player to the given coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    Game.prototype.pointTo = function (x, y) {
        if (!this.gameOver) {
            var angle = Maths.getAngle(0, 0, x - this._ground.width / 2, y - this._ground.height / 2);
            this._player.rotation = angle;
        }
    };
    Game.prototype.shoot = function () {
        if (!this.gameOver) {
            var gunCoordinates = this._player.getGunCoordinates();
            this._items.add(new Bullet(gunCoordinates.x, gunCoordinates.y, this._player.rotation));
        }
    };
    /**
     * Resize the ground of the game
     */
    Game.prototype.resize = function (width, height) {
        this._ground.width = width;
        this._ground.height = height;
    };
    /**
     * Process an iteration of moving
     */
    Game.prototype.iterate = function () {
        this._items.forEach(function (item) {
            if (item instanceof MovableItem)
                item.move();
        });
        this.processCollisions();
        this.processZombiesReactions();
    };
    /**
     * Process collisions between two items of the game
     */
    Game.prototype.processCollisions = function () {
        for (var i = 0; i < this._items.length; ++i) {
            var item1 = this._items.at(i);
            for (var j = 0; j < this._items.length; ++j) {
                if (j === i)
                    continue;
                var removeI = false;
                var removeJ = false;
                var item2 = this._items.at(j);
                if (item1.isTouching(item2)) {
                    if (item1 instanceof Zombie && item2 instanceof Human && item2.shield === false) {
                        this.endGame();
                    }
                    else if (item1 instanceof Bullet && item2 instanceof Zombie) {
                        removeJ = true;
                        removeI = true;
                        ++this.score;
                    }
                }
                if (removeJ) {
                    if (j < i)
                        i--;
                    this._items.remove(j);
                }
                if (removeI) {
                    this._items.remove(i);
                    j = 0;
                }
            }
        }
    };
    /**
     * Process zombies reactions
     */
    Game.prototype.processZombiesReactions = function () {
        for (var i = 0; i < this._items.length; ++i) {
            var item1 = this._items.at(i);
            if (item1 instanceof Zombie) {
                var distanceHuman = null;
                var humanToEat = null;
                for (var j = 0; j < this._items.length; ++j) {
                    if (i === j)
                        continue;
                    var item2 = this._items.at(j);
                    if (item2 instanceof Human) {
                        var distance = Maths.getDistance(item1.x, item1.y, item2.x, item2.y);
                        if (distanceHuman === null || distanceHuman > distance) {
                            distanceHuman = distance;
                            humanToEat = item2;
                        }
                    }
                }
                if (humanToEat)
                    item1.direction = Maths.getAngle(item1.x, item1.y, humanToEat.x, humanToEat.y);
            }
        }
    };
    /**
     * If a zombie touches the player, game is over
     */
    Game.prototype.endGame = function () {
        this._gameOver = true;
        this.stopTimerZombies();
    };
    /**
     * Draw the items of the game on a canvas
     * @param canvas Canvas to usinf for drawing
     */
    Game.prototype.draw = function (canvas) {
        var ctx = canvas.getContext('2d');
        ctx.save();
        ctx.translate(this._ground.width / 2, this._ground.height / 2);
        this._ground.draw(canvas);
        this._items.forEach(function (item) {
            item.draw(canvas);
        });
        ctx.restore();
    };
    return Game;
}());
//# sourceMappingURL=game.js.map