﻿/// <reference path="collection.ts" />
/// <reference path="drawable-item.ts" />
/// <reference path="movable-item.ts" />
/// <reference path="ground.ts" />
/// <reference path="human.ts" />
/// <reference path="zombie-child.ts" />
/// <reference path="zombie-scientist.ts" />
/// <reference path="bullet.ts" />

/**
 * Represents the game
 */
class Game
{
    /**
     * Ground of the game
     */
    private _ground: Ground;

    /**
     * Collection of drawable items (humans, zombies, bonus, ...)
     */
    private _items: Collection<DrawableItem>;

    /**
     * Player
     */
    private _player: Human;

    /**
     * Timer responsible for adding zonbie on the ground
     */
    private _timerZombies: number

    /**
     * Player score
     */
    private _score: number;
    public get score(): number { return this._score; };
    public set score(value: number) { this._score = value; };


    /**
     * Is game over ?
     */
    private _gameOver: boolean;
    public get gameOver(): boolean { return this._gameOver; };
    public set gameOver(value: boolean) { this._gameOver = value; };


    /**
     * Constructor
     */
    constructor()
    {
        this._items = new Collection<DrawableItem>();
        this._ground = new Ground();
        this._player = new Human(0, 0, Math.PI / 2);
        this._gameOver = false;
        this._score = 0;
    }

    /**
     * Initialize the game
     */
    initialize()
    {
        this._ground.initialize();

        this._items.add(this._player);

        for (let i = 0; i < 5; ++i)
            this.createZombie();

        this.startTimerZombies();
    }

    /**
     * Adds a zombie on the ground
     */
    createZombie()
    {
        let angle = Math.random() * Math.PI * 2;
        let x = (this._ground.width / 2 + 100) * Math.cos(angle);
        let y = (this._ground.width / 2 + 100) * Math.sin(angle);

        if (Math.random() < 0.5)
            this._items.add(new ZombieScientist(x, y, 0));
        else
            this._items.add(new ZombieChild(x, y, 0));
    }

    /**
     * Starts timer responsible for adding new zombies on the ground
     */
    startTimerZombies()
    {
        this.stopTimerZombies();

        if (!this.gameOver)
        {
            this._timerZombies = setTimeout(() =>
            {
                let zombiesNumber = Math.floor(Math.random() * 5 + 1);

                for (let i = 0; i < zombiesNumber; ++i)
                    this.createZombie();

                this.startTimerZombies();
            }, Math.random() * 1000 + 1000);
        }
    }

    /**
     * Stops timer responsible for adding zombies on the ground
     */
    stopTimerZombies()
    {
        clearTimeout(this._timerZombies);
        this._timerZombies = null;
    }

    /**
     * Angle the player to the given coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    pointTo(x, y)
    {
        if (!this.gameOver)
        {
            let angle = Maths.getAngle(0, 0, x - this._ground.width / 2, y - this._ground.height / 2);

            this._player.rotation = angle;
        }
    }

    shoot()
    {
        if (!this.gameOver)
        {
            let gunCoordinates = this._player.getGunCoordinates();

            this._items.add(new Bullet(gunCoordinates.x, gunCoordinates.y, this._player.rotation));
        }
    }

    /**
     * Resize the ground of the game
     */
    public resize(width: number, height: number)
    {
        this._ground.width = width;
        this._ground.height = height;
    }

    /**
     * Process an iteration of moving
     */
    public iterate()
    {
        this._items.forEach((item: DrawableItem) =>
        {
            if (item instanceof MovableItem)
                item.move();
        });

        this.processCollisions();
        this.processZombiesReactions();
    }

    /**
     * Process collisions between two items of the game
     */
    private processCollisions()
    {
        for (let i = 0; i < this._items.length; ++i)
        {
            let item1 = this._items.at(i);

            for (let j = 0; j < this._items.length; ++j)
            {
                if (j === i)
                    continue;

                let removeI = false;
                let removeJ = false;

                let item2 = this._items.at(j);

                if (item1.isTouching(item2))
                {
                    if (item1 instanceof Zombie && item2 instanceof Human && item2.shield === false)
                    {
                        this.endGame();
                    }
                    else if (item1 instanceof Bullet && item2 instanceof Zombie)
                    {
                        removeJ = true;
                        removeI = true;

                        ++this.score;
                    }
                }

                if (removeJ)
                {
                    if (j < i)
                        i--;
                    this._items.remove(j);
                }

                if (removeI)
                {
                    this._items.remove(i);
                    j = 0;
                }
            }
        }
    }

    /**
     * Process zombies reactions
     */
    private processZombiesReactions()
    {
        for (let i = 0; i < this._items.length; ++i)
        {
            let item1 = this._items.at(i);

            if (item1 instanceof Zombie)
            {
                let distanceHuman = null;
                let humanToEat = null;

                for (let j = 0; j < this._items.length; ++j)
                {
                    if (i === j)
                        continue;

                    let item2 = this._items.at(j);

                    if (item2 instanceof Human)
                    {
                        let distance = Maths.getDistance(item1.x, item1.y, item2.x, item2.y);


                        if (distanceHuman === null || distanceHuman > distance)
                        {
                            distanceHuman = distance;
                            humanToEat = item2;
                        }

                    }
                }

                if (humanToEat)
                    item1.direction = Maths.getAngle(item1.x, item1.y, humanToEat.x, humanToEat.y);
            }
        }
    }

    /**
     * If a zombie touches the player, game is over
     */
    private endGame()
    {
        this._gameOver = true;
        this.stopTimerZombies();
    }

    /**
     * Draw the items of the game on a canvas
     * @param canvas Canvas to usinf for drawing
     */
    public draw(canvas: HTMLCanvasElement)
    {
        let ctx = canvas.getContext('2d');
        ctx.save();
        ctx.translate(this._ground.width / 2, this._ground.height/2);
        

        this._ground.draw(canvas);
        this._items.forEach((item) =>
        {
            item.draw(canvas);
        });

        ctx.restore();
    }
}