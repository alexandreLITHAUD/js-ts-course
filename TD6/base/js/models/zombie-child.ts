﻿/// <reference path="zombie.ts" />
/// <reference path="textures-manager.ts" />

/**
 * Represent a Child Zombie
 */
class ZombieChild extends Zombie
{
    protected getTextureName() {
        return "zombie21";
    }
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     * @param direction Original direction of the zombie
     */
    constructor(x: number, y: number, direction: number)
    {
        super(x, y, direction);
    }
    
}