﻿/// <reference path="textures-manager.ts" />
/// <reference path="zombie.ts" />

/**
 * Represent a Scientist Zombie
 */
class ZombieScientist extends Zombie
{
    protected getTextureName() {
        return "zombie11";
    }
    /**
     * Constructor
     * @param x Original x coordinate
     * @param y Original y coordinate
     * @param direction Original direction of the zombie
     */
    constructor(x: number, y: number, direction: number)
    {
        super(x, y, direction);
    }


    
}