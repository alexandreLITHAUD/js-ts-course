/// <reference path="../../definitely-typed/es6-promise.d.ts" />
/// <reference path="map.ts" />
/**
 * Loads textures
 */
var TexturesManager = /** @class */ (function () {
    /**
     * Constructor
     */
    function TexturesManager() {
        this._textures = new Map();
    }
    /**
     * Return the instance of the singleton
     */
    TexturesManager.getInstance = function () {
        if (TexturesManager._instance === null)
            TexturesManager._instance = new TexturesManager();
        return TexturesManager._instance;
    };
    /**
     * Load a picture
     * @param path Path to the picture to load
     */
    TexturesManager.prototype.load = function (textureName, path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var image = new Image();
            image.onload = function () {
                _this._textures.setValue(textureName, image);
                resolve();
            };
            image.src = path;
        });
    };
    /**
     * Load all the needed textures for the game
     */
    TexturesManager.prototype.loadTextures = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var promises = new Array();
            promises.push(_this.load('ground', 'img/textures/ground.jpg'));
            promises.push(_this.load('bullets', 'img/textures/bullets.png'));
            promises.push(_this.load('dead', 'img/textures/dead.png'));
            promises.push(_this.load('zombie11', 'img/textures/zombie-1-1.png'));
            promises.push(_this.load('zombie12', 'img/textures/zombie-1-2.png'));
            promises.push(_this.load('zombie13', 'img/textures/zombie-1-3.png'));
            promises.push(_this.load('zombie14', 'img/textures/zombie-1-4.png'));
            promises.push(_this.load('zombie15', 'img/textures/zombie-1-5.png'));
            promises.push(_this.load('zombie16', 'img/textures/zombie-1-6.png'));
            promises.push(_this.load('zombie17', 'img/textures/zombie-1-7.png'));
            promises.push(_this.load('zombie18', 'img/textures/zombie-1-8.png'));
            promises.push(_this.load('zombie21', 'img/textures/zombie-2-1.png'));
            promises.push(_this.load('zombie22', 'img/textures/zombie-2-2.png'));
            promises.push(_this.load('zombie23', 'img/textures/zombie-2-3.png'));
            promises.push(_this.load('zombie24', 'img/textures/zombie-2-4.png'));
            promises.push(_this.load('zombie25', 'img/textures/zombie-2-5.png'));
            promises.push(_this.load('zombie26', 'img/textures/zombie-2-6.png'));
            promises.push(_this.load('zombie27', 'img/textures/zombie-2-7.png'));
            promises.push(_this.load('zombie28', 'img/textures/zombie-2-8.png'));
            promises.push(_this.load('human11', 'img/textures/human-1-1.png'));
            promises.push(_this.load('human12', 'img/textures/human-1-2.png'));
            promises.push(_this.load('human13', 'img/textures/human-1-3.png'));
            promises.push(_this.load('human14', 'img/textures/human-1-4.png'));
            promises.push(_this.load('human15', 'img/textures/human-1-5.png'));
            promises.push(_this.load('human16', 'img/textures/human-1-6.png'));
            promises.push(_this.load('human17', 'img/textures/human-1-7.png'));
            promises.push(_this.load('human18', 'img/textures/human-1-8.png'));
            promises.push(_this.load('ammo', 'img/textures/ammo.png'));
            promises.push(_this.load('shield', 'img/textures/shield.png'));
            promises.push(_this.load('glue', 'img/textures/glue.png'));
            promises.push(_this.load('speed', 'img/textures/speed.png'));
            Promise.all(promises).then(function () { resolve(); }).catch(function () { reject(); });
        });
    };
    /**
     * Return the texture which has the given name
     * @param textureName Name of the texture to return
     */
    TexturesManager.prototype.getTexture = function (textureName) {
        var texture = this._textures.getValue(textureName);
        return texture;
    };
    /**
     * Public instance of the singleton
     */
    TexturesManager._instance = null;
    return TexturesManager;
}());
//# sourceMappingURL=textures-manager.js.map