﻿
class View {

    constructor() {
        $("main").accordion({ active: false });

        $("#clear").on("click", () => { this.clearText(); this.activeSection(1); });

        $("#ok").on("click", () => { this.addText($("#type").val().toString(), $("#html").val()); this.activeSection(1); });
    }

    clearText() {
        $("#result").empty();
    }

    addText(type, text) {
        switch (type) {
            case "h1": $("#result").append("<h1>" + text + "</h1>"); break;

            case "h2": $("#result").append("<h2>" + text + "</h2>"); break;

            case "h3": $("#result").append("<h3>" + text + "</h3>"); break;

            case "p": $("#result").append("<p>" + text + "</p>"); break;

            case "code": $("#result").append("<script>" + text + "</script>"); break;

            default: break;
        }
    }

    activeSection(i) {
        $("main").accordion({ active: i });
    }

}