﻿/// <reference path="dbstorage.js" />
/// <reference path="jsonstorage.js" />
/**
 * Contrôleur (§ MVC)
 * */
class Controller extends Subject {


    constructor() {
        super();
        this.games = new Games(); // modèle
        this.storage = new DBStorage();
        this.storage.load(this.games);

    }

    /**
     * Ajoute un jeu     
     * @param {Game} game le jeu ajouté
     */
    addGame(game) {
        this.games.add(game);
        this.notify();
        this.storage.save(this.games);
    }

    /**
     * @returns {Games} le modèle
     * */
    getGames() {
        return this.games;
    }

    /**
     * @returns {Game} le jeu
     * @param {number} index l'indice
     */
    getGame(index) {
        return this.games.at(index);
    }

    /**
     * Met à jour le modèle : remplace un jeu
     * @param {any} index la position du jeu
     * @param {any} game le nouveau jeu
     */
    updateGame(index, game) {
        this.games.replace(index, game);
        this.notify();
        this.storage.save(this.games);
    }

    /**
     * Retire le jeu par sa position
     * @param {number} index la position du jeu
     */
    removeGame(index) {
        this.games.remove(index);
        this.notify();
        this.storage.save(this.games);
    }
}