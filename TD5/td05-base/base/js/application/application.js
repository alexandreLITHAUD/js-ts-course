﻿/**
 * Web application
 */
class Application
{
    /**
     * Constructor
     */
    constructor()
    {
        this.controler = new Controller();
        this.view = new View(this.controler);
        this.controler.addObserver(this.view);
        setTimeout(() => { this.view.refreshList(); }, 1000);
    }
}


/**
 * Start the application after the loading of the page is complete
 */
window.onload = function ()
{
    var application = new Application();
};