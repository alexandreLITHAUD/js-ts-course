﻿/**
 * Vue
 * @see Controller
 * */
class View extends Observer {
    /**
     * Initialise la vue
     * @param {Controller} ctrl le contrôleur associé
     */
    constructor(ctrl) {
        super();
        this.controller = ctrl;
        
        // gestion des évènements
        $("#btn-add").on("click", () => { this.clicAdd(); });
        $("#btn-update").on("click", () => {
            let index = this.getSelectionIndex();
            let game = this.createGame();
            this.controller.updateGame(index, game);
        });
        $("#btn-remove").on("click", () => {
            let index = this.getSelectionIndex();
            this.controller.removeGame(index);
        });

        this.notify();
    }
    /**
     * Définition de la notification : maj de l'affichage
     * */
    notify() {
        //console.log(this.controller);
        this.refreshList();
    }

    /**
     * Crée un jeu à partir des champs de saisie
     * @returns {Game} le jeu
     * */
    createGame() {
        let name = $("#txt-title").val();
        let year = $("#txt-release-year").val();
        let game = new Game();
        game.setReleaseYear(year);
        game.setTitle(name);
        return game;
    }

    /**
     * Réponse au clic sur le bouton ajouter
     * */
    clicAdd() {
        let game = this.createGame();
        this.controller.addGame(game);
    }

    /**
     * Rafraichit la liste des jeux à partir du modèle
     * */
    refreshList() {
        let games = this.controller.getGames();
        $("list").html(""); // supprime les enfants
        games.forEach((game) => {
            let div = document.createElement("div");
            div.classList.add("game");
            let p = document.createElement("p");
            p.classList.add("title");
            p.innerHTML = game.getTitle();
            div.appendChild(p);
            p = document.createElement("p");
            p.classList.add("info");
            p.innerHTML = game.getReleaseYear();
            div.appendChild(p);
            $("list").append(div);
			div.onclick = ()=>{
				$(".game").removeClass("selected");
				div.classList.add("selected");
				this.displayGame(game);
			};
			
        });
    }

    /**
     * Affiche les données d'un jeu
     * @param {Game} game le jeu
     */
    displayGame(game) {
        $("#txt-title").val(game.getTitle());
        $("#txt-release-year").val(game.getReleaseYear());
    }

    /**
     * Indique l'index du jeu sélectionné
     * @returns {number} l'indice (-1 si aucun) du jeu sélectionné
     * */
    getSelectionIndex() {
        let select = $(".selected");
        return $(".game").index(select);
    }
}