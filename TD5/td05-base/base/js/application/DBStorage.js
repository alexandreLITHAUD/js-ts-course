﻿class DBStorage {

    createDatabase(db) {
        var store = db.createObjectStore("games", { autoIncrement: "true" });
        db.close();
    }

    save(games) {
        var bd = window.indexedDB.open("games", 1);

        bd.onupgradeneeded = (event) => {
            var db = event.target.result;
            this.createDatabase(db);
        };

        bd.onsuccess = (event) => {
            var db = event.target.result;
            var transaction = db.transaction(["games"], "readwrite");
            transaction.onerror = (event) => {
                console.log(event);
            };
            var store = transaction.objectStore("games");
            var request = store.clear();
            request.onsuccess = () => {
                games.forEach((game) => {
                    var request2 = store.add(game);
                });
            };
        };
    }

    load(games) {
        var bd = window.indexedDB.open("games", 1);
        bd.onupgradeneeded = (event) => {
            vardb = event.target.result; this.createDatabase(db);
        };
        bd.onsuccess = (event) => {
            var db = event.target.result;
            var transaction = db.transaction(["games"]);
            var store = transaction.objectStore("games");
            var tab = [];
            store.openCursor().onsuccess = (event) => {
                var cursor = event.target.result;
                if (cursor) {
                    tab.push(cursor.value);
                    cursor.continue();
                } else {
                    games.hydrate(tab);
                    db.close();
                }
            };
        }; 
    }       
}