﻿class JSONStorage {

    save(games) {
        window.localStorage.setItem("GAMES", JSON.stringify(games));
    }

    /**
     * 
     * @param {Games} games
     */
    load(games) {
        let str = window.localStorage.getItem("GAMES");

        if (str) {
            let tab = JSON.parse(str);
            games.hydrate(tab.items);
        }
    }

}