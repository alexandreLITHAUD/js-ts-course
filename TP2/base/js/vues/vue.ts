﻿/// <reference path="../controleurs/controleur.ts" />
/// <reference path="../libs/jquery.d.ts" />

/**
 * Gestion de la vue principale de l'application
 */
class Vue
{
	//Controleur associé à la vue
	private controleur: Controleur = null;

	/**
	 * Constructeur
	 */
	constructor()
	{
	}

	/**
	 * Initialisation de la vue
	 * @param {Controleur} controleur Controleur associé à la vue
	 */
	initialiser(controleur: Controleur)
	{
	  this.controleur = controleur;
	  this.attacherEvenements();
	}

	/**
	 * @todo Attache les événements click aux boutons de la page HTML
	 */
	attacherEvenements()
	{
		// Appuis du bouton de commande des matières premières
		$("#btn-commander-matieres-premieres").on("click", () => {
			try {
				this.controleur.commanderMatieresPremieres();
			} catch (e) {
				this.afficherErreur(e.message);
            }		
		});

		// Bouton de recrutement
		$("#btn-recruter").on("click", () => {
			try {
				this.controleur.recruter();
			} catch (e) {
				this.afficherErreur(e.message);
            }	
		});

		// Bouton de licenciement
		$("#btn-licencier").on("click", () => {
			try {
				this.controleur.licencier();
			} catch (e) {
				this.afficherErreur(e.message);
            }		
		});

		// Bouton de production d'un velo
		$("#btn-produire-velo").on("click", () => {
			try {
				this.controleur.produireVelo();
			} catch (e) {
				this.afficherErreur(e.message);
            }	
		});

		// Bouton de production d'un scooter
		$("#btn-produire-scooter").on("click", () => {
			try {
				this.controleur.produireScooter();
			} catch (e) {
				this.afficherErreur(e.message);
            }	
		});

		// Bouton de production d'une voiture
		$("#btn-produire-voiture").on("click", () => {
			try {
				this.controleur.produireVoiture();
			} catch (e) {
				this.afficherErreur(e.message);
            }
			
		});
	}

	/**
	 * Actualise l'affichage de la page
	 * @param entreprise {Entreprise} Informations sur l'entreprise
	 * @param consommateurs {Consommateurs} Informations sur les consommateurs
	 */
	actualiser(entreprise: Entreprise, consommateurs: Consommateurs)
	{
		//Actualisation des ressources
		this.actualiserRessources(entreprise);

		//Actualisation de la production
		this.actualiserProduction(entreprise);

		//Actualisation du stock
		this.actualiserStock(entreprise);

		//Actualisation du stock
		this.actualiserDemandes(consommateurs);
	}

	/**
	 * @todo Actualise le niveau des ressources
	 * @param entreprise {Entreprise} Entreprise pour laquelle sont affichées les ressources
	 */
	actualiserRessources(entreprise: Entreprise)
	{
		$("#matieres-premieres .header-ressource-quantite").text(entreprise.getMatieresPremieres());
		$("#stock .header-ressource-quantite").text(entreprise.getEspaceStockageOccupe() + "/" + entreprise.getEspaceStockageTotal());
		$("#ressources-humaines .header-ressource-quantite").text(entreprise.getRessourcesHumainesOccupees() + "/" + entreprise.getRessourcesHumainesTotal());
		$("#tresorerie .header-ressource-quantite").text(entreprise.getTresorerie());
	}

	/**
	 * @todo Actualise la production en cours
	 * @param entreprise {Entreprise} Entreprise pour laquelle sont affichées informations de production
	 */
	actualiserProduction(entreprise: Entreprise)
	{
		$("#cadre-production #info-production-velo").text(entreprise.getQuantiteProductionVelos());
		$("#cadre-production #info-production-scooter").text(entreprise.getQuantiteProductionScooters());
		$("#cadre-production #info-production-voiture").text(entreprise.getQuantiteProductionVoitures());
	}

	/**
	 * @todo Actualise le stock
	 * @param entreprise {Entreprise} Entreprise pour laquelle est affiché l'état du stock
	 */
	actualiserStock(entreprise: Entreprise)
	{
		$("#cadre-stock #info-stock-velo").text(entreprise.getQuantiteStockVelos());
		$("#cadre-stock #info-stock-scooter").text(entreprise.getQuantiteStockScooters());
		$("#cadre-stock #info-stock-voiture").text(entreprise.getQuantiteStockVoitures());
	}

	/**
	 * @todo Actualise la demande des consommateurs
	 * @param consommateurs {Consommateurs} Consommateurs pour lesquels sont affichées les demandes en produits
	 */
	actualiserDemandes(consommateurs: Consommateurs)
	{
		$("#cadre-demandes #info-demandes-velo").text(consommateurs.getDemandeVelo());
		$("#cadre-demandes #info-demandes-scooter").text(consommateurs.getDemandeScooter());
		$("#cadre-demandes #info-demandes-voiture").text(consommateurs.getDemandeVoiture());

	}

	//#region Gestion des alertes
	/**
	 * Affiche une erreur
	 * @param erreur {string} Erreur à afficher
	 */
	afficherErreur(erreur: string)
	{
		this.afficherAlerte(erreur, 'erreur');
	}

	/**
	 * Affiche une actualité
	 * @param actualite {string} Titre de l'actualité à afficher
	 */
	afficherActualite(actualite: string)
	{
		this.afficherAlerte(actualite, 'actualite');
	}

	/**
	 * Affiche l'écran de fin de partie
	 */
	afficherFinPartie()
	{
		$('#ecran-fin-partie').addClass('visible');
	}

	/**
	 * Affiche une alerte
	 * @param titre {string} Texte de l'alerte
	 * @param type {string} Type d'alerte (erreur ou actualite)
	 */
	afficherAlerte(titre: string, type: string)
	{
		var alerte = $('<p></p>');
		alerte.addClass(type);
		alerte.html(titre);

		$('#alertes').append(alerte);

		//Efface l'alerte après 5 secondes
		setTimeout(() => { this.controleur.retirerAlerte(); }, 5000);
	}

	/**
	 * Efface la première alerte de la page
	 */
	retirerAlerte()
	{
		$('#alertes p').eq(0).remove();
	}
	//#endregion
}