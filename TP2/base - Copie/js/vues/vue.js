/// <reference path="../controleurs/controleur.ts" />
/// <reference path="../libs/jquery.d.ts" />
/**
 * Gestion de la vue principale de l'application
 */
var Vue = /** @class */ (function () {
    /**
     * Constructeur
     */
    function Vue() {
        //Controleur associé à la vue
        this.controleur = null;
    }
    /**
     * Initialisation de la vue
     * @param {Controleur} controleur Controleur associé à la vue
     */
    Vue.prototype.initialiser = function (controleur) {
        this.controleur = controleur;
        this.attacherEvenements();
    };
    /**
     * @todo Attache les événements click aux boutons de la page HTML
     */
    Vue.prototype.attacherEvenements = function () {
        var _this = this;
        // Appuis du bouton de commande des matières premières
        $("#btn-commander-matieres-premieres").on("click", function () {
            try {
                _this.controleur.commanderMatieresPremieres();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
        // Bouton de recrutement
        $("#btn-recruter").on("click", function () {
            try {
                _this.controleur.recruter();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
        // Bouton de licenciement
        $("#btn-licencier").on("click", function () {
            try {
                _this.controleur.licencier();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
        // Bouton de production d'un velo
        $("#btn-produire-velo").on("click", function () {
            try {
                _this.controleur.produireVelo();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
        // Bouton de production d'un scooter
        $("#btn-produire-scooter").on("click", function () {
            try {
                _this.controleur.produireScooter();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
        // Bouton de production d'une voiture
        $("#btn-produire-voiture").on("click", function () {
            try {
                _this.controleur.produireVoiture();
            }
            catch (e) {
                _this.afficherErreur(e.message);
            }
        });
    };
    /**
     * Actualise l'affichage de la page
     * @param entreprise {Entreprise} Informations sur l'entreprise
     * @param consommateurs {Consommateurs} Informations sur les consommateurs
     */
    Vue.prototype.actualiser = function (entreprise, consommateurs) {
        //Actualisation des ressources
        this.actualiserRessources(entreprise);
        //Actualisation de la production
        this.actualiserProduction(entreprise);
        //Actualisation du stock
        this.actualiserStock(entreprise);
        //Actualisation du stock
        this.actualiserDemandes(consommateurs);
    };
    /**
     * @todo Actualise le niveau des ressources
     * @param entreprise {Entreprise} Entreprise pour laquelle sont affichées les ressources
     */
    Vue.prototype.actualiserRessources = function (entreprise) {
        $("#matieres-premieres .header-ressource-quantite").text(entreprise.getMatieresPremieres());
        $("#stock .header-ressource-quantite").text(entreprise.getEspaceStockageOccupe() + "/" + entreprise.getEspaceStockageTotal());
        $("#ressources-humaines .header-ressource-quantite").text(entreprise.getRessourcesHumainesOccupees() + "/" + entreprise.getRessourcesHumainesTotal());
        $("#tresorerie .header-ressource-quantite").text(entreprise.getTresorerie());
    };
    /**
     * @todo Actualise la production en cours
     * @param entreprise {Entreprise} Entreprise pour laquelle sont affichées informations de production
     */
    Vue.prototype.actualiserProduction = function (entreprise) {
        $("#cadre-production #info-production-velo").text(entreprise.getQuantiteProductionVelos());
        $("#cadre-production #info-production-scooter").text(entreprise.getQuantiteProductionScooters());
        $("#cadre-production #info-production-voiture").text(entreprise.getQuantiteProductionVoitures());
    };
    /**
     * @todo Actualise le stock
     * @param entreprise {Entreprise} Entreprise pour laquelle est affiché l'état du stock
     */
    Vue.prototype.actualiserStock = function (entreprise) {
        $("#cadre-stock #info-stock-velo").text(entreprise.getQuantiteStockVelos());
        $("#cadre-stock #info-stock-scooter").text(entreprise.getQuantiteStockScooters());
        $("#cadre-stock #info-stock-voiture").text(entreprise.getQuantiteStockVoitures());
    };
    /**
     * @todo Actualise la demande des consommateurs
     * @param consommateurs {Consommateurs} Consommateurs pour lesquels sont affichées les demandes en produits
     */
    Vue.prototype.actualiserDemandes = function (consommateurs) {
        $("#cadre-demandes #info-demandes-velo").text(consommateurs.getDemandeVelo());
        $("#cadre-demandes #info-demandes-scooter").text(consommateurs.getDemandeScooter());
        $("#cadre-demandes #info-demandes-voiture").text(consommateurs.getDemandeVoiture());
    };
    //#region Gestion des alertes
    /**
     * Affiche une erreur
     * @param erreur {string} Erreur à afficher
     */
    Vue.prototype.afficherErreur = function (erreur) {
        this.afficherAlerte(erreur, 'erreur');
    };
    /**
     * Affiche une actualité
     * @param actualite {string} Titre de l'actualité à afficher
     */
    Vue.prototype.afficherActualite = function (actualite) {
        this.afficherAlerte(actualite, 'actualite');
    };
    /**
     * Affiche l'écran de fin de partie
     */
    Vue.prototype.afficherFinPartie = function () {
        $('#ecran-fin-partie').addClass('visible');
    };
    /**
     * Affiche une alerte
     * @param titre {string} Texte de l'alerte
     * @param type {string} Type d'alerte (erreur ou actualite)
     */
    Vue.prototype.afficherAlerte = function (titre, type) {
        var _this = this;
        var alerte = $('<p></p>');
        alerte.addClass(type);
        alerte.html(titre);
        $('#alertes').append(alerte);
        //Efface l'alerte après 5 secondes
        setTimeout(function () { _this.controleur.retirerAlerte(); }, 5000);
    };
    /**
     * Efface la première alerte de la page
     */
    Vue.prototype.retirerAlerte = function () {
        $('#alertes p').eq(0).remove();
    };
    return Vue;
}());
//# sourceMappingURL=vue.js.map