﻿/// <reference path="articles.ts" />
/// <reference path="article.ts" />
/// <reference path="subject.ts" />

class Controler extends Subject {

    private articles: Articles;

    public loadArticles() {

        this.articles = new Articles();

        $.ajax({
            url: "php/getArticles.php",
            method: "post",
            dataType: "json",
            success: (obj) => {
                obj.forEach((elt) => {
                    let title = elt.title;
                    let content = elt.content;
                    let article = new Article(title, content);
                    this.articles.add(article);
                });
                this.notify(this.articles);
            },
            error: (obj) => {
                console.log(obj);
            }
        });
    }

}
