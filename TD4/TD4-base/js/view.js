/// <reference path="observer.ts" />
/// <reference path="controler.ts" />
/// <reference path="../definitely-typed/jquery.d.ts" />
var View = /** @class */ (function () {
    function View(c) {
        this.ctrl = c;
        this.ctrl.register(this);
        this.attachEvents();
    }
    View.prototype.update = function (list) {
        $("ul.onglets").empty();
        $("div.panels").empty();
        for (var i = 0; i < list.size(); i++) {
            var a = list.get(i);
            var onglet = document.createElement("li");
            onglet.classList.add("onglet");
            onglet.innerHTML = a.Title;
            $("ul.onglets").append(onglet);
            var panel = document.createElement("div");
            panel.classList.add("panelOnglet");
            panel.innerHTML = a.Content;
            $("div.panels").append(panel);
        }
        $(".onglet").eq(0).addClass("actif");
        $(".panelOnglet").eq(0).addClass("actif");
        this.attachEvents();
    };
    View.prototype.attachEvents = function () {
        var _this = this;
        $(".onglet").on("click", function (evt) {
            _this.selectTab(evt.target);
        });
    };
    View.prototype.selectTab = function (elt) {
        $(".onglet").removeClass("actif");
        elt.classList.add("actif");
        $(".panelOnglet").removeClass("actif");
        var index = $(".onglet").index(elt);
        $(".panelOnglet").eq(index).addClass("actif");
    };
    return View;
}());
//# sourceMappingURL=view.js.map