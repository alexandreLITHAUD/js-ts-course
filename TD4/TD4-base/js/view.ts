﻿/// <reference path="observer.ts" />
/// <reference path="controler.ts" />
/// <reference path="../definitely-typed/jquery.d.ts" />

class View implements Observer {

    update(list: Articles) {
        $("ul.onglets").empty();
        $("div.panels").empty();

        for (let i = 0; i < list.size(); i++) {
            let a = list.get(i);

            let onglet = document.createElement("li");
            onglet.classList.add("onglet");
            onglet.innerHTML = a.Title;
            $("ul.onglets").append(onglet);

            let panel = document.createElement("div");
            panel.classList.add("panelOnglet");
            panel.innerHTML = a.Content;
            $("div.panels").append(panel);
        }

        $(".onglet").eq(0).addClass("actif");
        $(".panelOnglet").eq(0).addClass("actif");
        this.attachEvents();

    }

    private ctrl: Controler;

    public constructor(c: Controler) {
        this.ctrl = c;
        this.ctrl.register(this);
        this.attachEvents();
    }

    private attachEvents() {
        $(".onglet").on("click", (evt) => {
            this.selectTab(evt.target);
        });
    }

    private selectTab(elt: Element) {
        $(".onglet").removeClass("actif");
        elt.classList.add("actif");

        $(".panelOnglet").removeClass("actif");

        let index = $(".onglet").index(elt);
        $(".panelOnglet").eq(index).addClass("actif");
    }
}

