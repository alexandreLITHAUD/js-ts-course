﻿/**
 * Compose des Article
 * */
class Articles {
    private articles: Article[];

    public constructor() {
        this.articles = [];
    }

    /**
     * Ajoute un article à la collection    
     * @param art l'article à ajouter
     */
    public add(art: Article) {
        this.articles.push(art);
    }

    /** 
     * Vide la collection
     * */
    public clear() {
        this.articles = [];
    }

    /**
     * Fournit l'article n°i (0=premier)     
     * @param i l'indice de l'article
     * @throws si i n'est pas correct (hors limites)
     * @returns l'article n°i
     */
    public get(i: number): Article {
        if (i < 0 || i > this.size())
            throw "Indice hors limites";
        return this.articles[i];
    }

    /**
     * @returns le nombre d'articles
     * */
    public size(): number {
        return this.articles.length;
    }
}