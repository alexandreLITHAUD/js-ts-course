/**
 * Compose des Article
 * */
var Articles = /** @class */ (function () {
    function Articles() {
        this.articles = [];
    }
    /**
     * Ajoute un article à la collection
     * @param art l'article à ajouter
     */
    Articles.prototype.add = function (art) {
        this.articles.push(art);
    };
    /**
     * Vide la collection
     * */
    Articles.prototype.clear = function () {
        this.articles = [];
    };
    /**
     * Fournit l'article n°i (0=premier)
     * @param i l'indice de l'article
     * @throws si i n'est pas correct (hors limites)
     * @returns l'article n°i
     */
    Articles.prototype.get = function (i) {
        if (i < 0 || i > this.size())
            throw "Indice hors limites";
        return this.articles[i];
    };
    /**
     * @returns le nombre d'articles
     * */
    Articles.prototype.size = function () {
        return this.articles.length;
    };
    return Articles;
}());
//# sourceMappingURL=articles.js.map