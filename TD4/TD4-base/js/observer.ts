﻿/**
 * Interface pour le pattern observateur, utilisé par le MVC
 * */
interface Observer {
    /**
     * The view need to update
     * @param list les articles à afficher (le modèle)
     * */
    update(list: Articles); 
}