﻿/// <reference path="observer.ts" />
/// <reference path="articles.ts" />
/// <reference path="../definitely-typed/jquery.d.ts" />


/**
 * Observable du pattern observateur */
class Subject {
    private observers: Observer[];

    public constructor() {
        this.observers = [];
    }

    /** 
     * Register an observer     
     * @param o
     */
    public register(o: Observer) {
        this.observers.push(o);
    }

    /**
     * Unregister all the observers*/
    public unregisterAll() 
    {
        this.observers = [];
    }

    /**
     * Notify the observers to update views
     * @param list les articles à afficher
     * */
    protected notify(list: Articles) {
        this.observers.forEach((o) => { o.update(list); });
    }
}