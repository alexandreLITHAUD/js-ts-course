/// <reference path="observer.ts" />
/// <reference path="articles.ts" />
/// <reference path="../definitely-typed/jquery.d.ts" />
/**
 * Observable du pattern observateur */
var Subject = /** @class */ (function () {
    function Subject() {
        this.observers = [];
    }
    /**
     * Register an observer
     * @param o
     */
    Subject.prototype.register = function (o) {
        this.observers.push(o);
    };
    /**
     * Unregister all the observers*/
    Subject.prototype.unregisterAll = function () {
        this.observers = [];
    };
    /**
     * Notify the observers to update views
     * @param list les articles à afficher
     * */
    Subject.prototype.notify = function (list) {
        this.observers.forEach(function (o) { o.update(list); });
    };
    return Subject;
}());
//# sourceMappingURL=subject.js.map