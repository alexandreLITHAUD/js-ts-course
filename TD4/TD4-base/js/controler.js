/// <reference path="articles.ts" />
/// <reference path="article.ts" />
/// <reference path="subject.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Controler = /** @class */ (function (_super) {
    __extends(Controler, _super);
    function Controler() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Controler.prototype.loadArticles = function () {
        var _this = this;
        this.articles = new Articles();
        $.ajax({
            url: "php/getArticles.php",
            method: "post",
            dataType: "json",
            success: function (obj) {
                obj.forEach(function (elt) {
                    var title = elt.title;
                    var content = elt.content;
                    var article = new Article(title, content);
                    _this.articles.add(article);
                });
                _this.notify(_this.articles);
            },
            error: function (obj) {
                console.log(obj);
            }
        });
    };
    return Controler;
}(Subject));
//# sourceMappingURL=controler.js.map