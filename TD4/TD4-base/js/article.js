/**
 * Article de journal
 * */
var Article = /** @class */ (function () {
    /**
     * Initialise l'article
     * @param title le titre de l'article
     * @param content le contenu de l'article
     */
    function Article(title, content) {
        this.title = title || '<vide>';
        this.content = content || '';
    }
    Object.defineProperty(Article.prototype, "Title", {
        // propriétés    
        get: function () { return this.title; },
        set: function (value) {
            if (value === undefined || value === null || value === "")
                throw "Le titre doit être défini";
            this.title = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Article.prototype, "Content", {
        get: function () { return this.content; },
        set: function (value) {
            this.content = value;
        },
        enumerable: false,
        configurable: true
    });
    return Article;
}());
//# sourceMappingURL=article.js.map