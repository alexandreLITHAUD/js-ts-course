﻿/**
 * Article de journal
 * */
class Article {
    private title: string;
    private content: string;

    /**
     * Initialise l'article
     * @param title le titre de l'article
     * @param content le contenu de l'article
     */
    public constructor(title: string, content: string) {
        this.title = title || '<vide>';
        this.content = content || '';
    }

    // propriétés    
    public get Title() : string { return this.title; }
    public set Title(value: string) {
        if (value === undefined || value === null || value === "")
            throw "Le titre doit être défini";
        this.title = value;
    }

    public get Content(): string { return this.content; }
    public set Content(value: string) {
        this.content = value;
    }

}