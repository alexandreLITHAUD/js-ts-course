<?php

$articles = array(
    array(
        "title"=>"Date et heure",
        "content"=> date("H:i:s d/m/Y")
    ),

    array(
        "title" => "Smartphones en baisse",
        "content" => "Le marché de l'électronique grand public français a réservé quelques surprises. En 2016, les ventes de smartphones ont baissé pour la première fois depuis l'avènement de cette nouvelle catégorie née avec le lancement de l'iPhone en 2007. Les ventes ont chuté de 6 % pour s'établir à 20 millions d'appareils vendus, soit un chiffre d'affaires de 3,7 milliards d'euros, selon le cabinet d'études GfK. Ce retournement marque un véritable changement pour ce secteur qui atteint un stade de maturité. La France est d'ailleurs un des premiers pays où il est aussi sensible. À l'échelle mondiale, la croissance des ventes ralentit, mais elle ne baisse pas."
    ),
    array(
        "title" => "Record pour l'iPhone",
        "content" => "Décevant l’iPhone 7 ? Pas si sûr que ça, si l’on en croit les chiffres de ventes communiqués par Apple ce 31 janvier. La société estime en effet avoir vendu 78,3 millions d’exemplaires de smartphones (tous modèles confondus) au quatrième trimestre 2016 ; traditionnellement le plus important de l’année. Cela établit un nouveau record de vente du téléphone depuis son lancement en 2007.<br /><br />Autre nouveau record pour Apple – et qui ne va pas dans le sens du consommateur – l’iPhone n’a jamais été aussi cher. Il coûte ainsi en moyenne 695 dollars, en augmentation de 4 dollars par rapport au même trimestre de 2015."
    ),
    array(
        "title" => "Plan Très Haut Débit",
        "content" => "Si on écoute le gouvernement, la couverture de la France en fibre optique est sur les rails. Il suffit pour s'en convaincre de regarder les spots TV diffusés tous les jours sur plusieurs chaînes démontrant l'intérêt du très haut débit et surtout l'avancée des déploiements. Le tout avec de belles images de techniciens déroulant des tuyaux dans les champs... <br /><br />Pourtant, selon les derniers chiffres du régulateur, le nombre d'abonnements au très haut débit atteint 5 millions au 30 septembre 2016, ce qui représente seulement 18% des accès Internet en France. Rappelons que selon la définition adoptée par l’ARCEP et l’Union Européenne, le Très Haut Débit correspond à un minimum de 30 Mb/s de débit descendant. La majorité de la croissance est à mettre au crédit du FTTH, : sa progression est +775.000 en un an pour atteindre 1,9 million d’abonnés."
    ),
);

echo json_encode($articles);