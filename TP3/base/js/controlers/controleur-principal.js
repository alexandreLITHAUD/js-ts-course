/// <reference path="../views/vue-principale.ts" />
/// <reference path="../patterns/observateur.ts" />
/// <reference path="../models/jeu.ts" />
/**
 * Controleur de l'application
 */
var ControleurPrincipal = /** @class */ (function () {
    /**
     * Constructeur
     */
    function ControleurPrincipal() {
    }
    ControleurPrincipal.prototype.initialiser = function () {
        this.vue = new VuePrincipale(this);
        this.jeu = new Jeu(this);
        this.timerRafraichissement = null;
        //Redimensionne la zone de dessin
        this.redimensionner();
        //Affiche l'écran de démarrage
        this.vue.afficherDebutPartie();
    };
    /**
     * Fonction de notification appelée par les sujets du controleur
     */
    ControleurPrincipal.prototype.notifier = function () {
        //Actualisation de la vue
        this.vue.actualiser(this.jeu);
        //Test permettant de savoir si le jeu est terminé ou s'il continue
        if (this.jeu.estTermine())
            this.terminerJeu();
    };
    /**
     * Redimensionnement les éléments en fonction de la taille de l'écran
     */
    ControleurPrincipal.prototype.redimensionner = function () {
        this.vue.redimensionner();
        this.jeu.setDimensionsPlateau(this.vue.getLargeurDessin(), this.vue.getHauteurDessin());
    };
    /**
     * Démarre une nouvelle partie
     */
    ControleurPrincipal.prototype.commencerNouveauJeu = function () {
        this.vue.masquerBandeaux();
        this.jeu.nouveau();
        this.animer();
    };
    /**
     * Poursuit la partie sur un nouveau niveau
     */
    ControleurPrincipal.prototype.commencerNiveauSuivant = function () {
        this.vue.masquerBandeaux();
        this.jeu.niveauSuivant();
        this.animer();
    };
    /**
     * Termine le niveau en cours
     */
    ControleurPrincipal.prototype.terminerJeu = function () {
        clearTimeout(this.timerRafraichissement);
        //Gestion de l'affichage en fonction de la manière dont s'est terminé le niveau (gain ou perte)
        if (this.jeu.estGagne())
            this.vue.afficherPartieGagnee();
        else
            this.vue.afficherPartiePerdue();
    };
    /**
     * Rafraichissement du jeu toutes les 40 milisecondes
     */
    ControleurPrincipal.prototype.animer = function () {
        var _this = this;
        this.jeu.animer();
        if (!this.jeu.estTermine())
            this.timerRafraichissement = setTimeout(function () { _this.animer(); }, 40);
    };
    /**
     * Gestionnaire d'événement appelé lorsque l'utilisateur clique sur le canvas
     */
    ControleurPrincipal.prototype.onMouseDown = function () {
        if (!this.jeu.estTermine())
            this.jeu.tirer();
    };
    /**
     * Gestionnaire d'événement appelé lorsque l'utilisateur bouge la souris au dessus du canvas
     */
    ControleurPrincipal.prototype.onMouseMove = function (coordonnees) {
        if (!this.jeu.estTermine())
            this.jeu.orienterJoueurVers(coordonnees);
    };
    return ControleurPrincipal;
}());
//# sourceMappingURL=controleur-principal.js.map