﻿/// <reference path="../views/vue-principale.ts" />
/// <reference path="../patterns/observateur.ts" />
/// <reference path="../models/jeu.ts" />


/**
 * Controleur de l'application
 */
class ControleurPrincipal implements Observateur
{
  //Vue principale de l'application
  private vue: VuePrincipale;

  //Gestion du jeu
  private jeu: Jeu;

  //Timer utilisé pour le refraichissement de l'écran
  private timerRafraichissement: number;

  /**
   * Constructeur
   */
  constructor()
  {
  }

  initialiser()
  {
	this.vue = new VuePrincipale(this);
	this.jeu = new Jeu(this);
	this.timerRafraichissement = null;

	//Redimensionne la zone de dessin
	this.redimensionner();

	//Affiche l'écran de démarrage
	this.vue.afficherDebutPartie();
  }

  /**
   * Fonction de notification appelée par les sujets du controleur
   */
  notifier()
  {
	//Actualisation de la vue
	this.vue.actualiser(this.jeu);

	//Test permettant de savoir si le jeu est terminé ou s'il continue
	if (this.jeu.estTermine())
	  this.terminerJeu();
  }

  /**
   * Redimensionnement les éléments en fonction de la taille de l'écran
   */
  redimensionner()
  {
	this.vue.redimensionner();
	this.jeu.setDimensionsPlateau(this.vue.getLargeurDessin(), this.vue.getHauteurDessin());
  }

  /**
   * Démarre une nouvelle partie
   */
  commencerNouveauJeu()
  {
	this.vue.masquerBandeaux();
	this.jeu.nouveau();
	this.animer();
  }

  /**
   * Poursuit la partie sur un nouveau niveau
   */
  commencerNiveauSuivant()
  {
	this.vue.masquerBandeaux();
	this.jeu.niveauSuivant();
	this.animer();
  }

  /**
   * Termine le niveau en cours
   */
  terminerJeu()
  {
	clearTimeout(this.timerRafraichissement);

	//Gestion de l'affichage en fonction de la manière dont s'est terminé le niveau (gain ou perte)
	if (this.jeu.estGagne())
	  this.vue.afficherPartieGagnee();
	else
	  this.vue.afficherPartiePerdue();
  }

  /**
   * Rafraichissement du jeu toutes les 40 milisecondes
   */
  animer()
  {
	this.jeu.animer();

	if (!this.jeu.estTermine())
	  this.timerRafraichissement = setTimeout(() => { this.animer(); }, 40);
  }

  /**
   * Gestionnaire d'événement appelé lorsque l'utilisateur clique sur le canvas
   */
  onMouseDown()
  {
	if (!this.jeu.estTermine())
	  this.jeu.tirer();
  }


  /**
   * Gestionnaire d'événement appelé lorsque l'utilisateur bouge la souris au dessus du canvas
   */
  onMouseMove(coordonnees)
  {
	if (!this.jeu.estTermine())
	  this.jeu.orienterJoueurVers(coordonnees);
  }
}