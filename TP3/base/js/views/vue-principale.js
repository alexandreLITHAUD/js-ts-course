/// <reference path="../patterns/sujet.ts" />
/// <reference path="../controlers/controleur-principal.ts" />
/// <reference path="../models/point.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Vue principale de l'application
 */
var VuePrincipale = /** @class */ (function (_super) {
    __extends(VuePrincipale, _super);
    /**
     * Constructeur
     */
    function VuePrincipale(controleur) {
        var _this = _super.call(this) || this;
        _this.controleur = controleur;
        _this.canvas = $('#canvas-dessin')[0];
        _this.context = _this.canvas.getContext('2d');
        _this.attacherEvenements();
        return _this;
    }
    /**
     * Attache les événements aux éléments de la page Web
     */
    VuePrincipale.prototype.attacherEvenements = function () {
        var _this = this;
        $('#btnStart').bind('click', function () { _this.controleur.commencerNouveauJeu(); });
        $('#btnReStart').bind('click', function () { _this.controleur.commencerNouveauJeu(); });
        $('#btnNextLevel').bind('click', function () { _this.controleur.commencerNiveauSuivant(); });
        $(this.canvas).bind('mousemove', function (event) {
            var canvasPosition = $(_this.canvas).offset();
            var coordonnees = new Point(event.clientX - canvasPosition.left, event.clientY - canvasPosition.top + $(window).scrollTop());
            _this.controleur.onMouseMove(coordonnees);
        });
        $(this.canvas).bind('mousedown', function () {
            _this.controleur.onMouseDown();
        });
    };
    /**
     * Retourne la largeur de la zone de dessin
     * @returns {number} La largeur de la zone de dessin
     */
    VuePrincipale.prototype.getLargeurDessin = function () {
        return this.canvas.width;
    };
    /**
     * Retourne la hauteur de la zone de dessin
     * @returns {number} La hauteur de la zone de dessin
     */
    VuePrincipale.prototype.getHauteurDessin = function () {
        return this.canvas.height;
    };
    /**
     * Redimensionne les éléments de la page en fonction de la taille de la fenêtre
     */
    VuePrincipale.prototype.redimensionner = function () {
        this.canvas.width = $(this.canvas).width();
        this.canvas.height = $(this.canvas).height();
        var marginTop = ($(window).height() - $('.bandeau').height()) / 2;
        $('.bandeau').css('margin-top', marginTop);
    };
    /**
     * Affiche le calque de début de partie
     */
    VuePrincipale.prototype.afficherDebutPartie = function () {
        $('#debut-partie').show();
    };
    /**
     * Affiche le calque de fin de partie (perdue)
     */
    VuePrincipale.prototype.afficherPartiePerdue = function () {
        $('#partie-perdue').show();
    };
    /**
     * Affiche le calque de fin de niveau (gagné)
     */
    VuePrincipale.prototype.afficherPartieGagnee = function () {
        $('#partie-gagnee').show();
    };
    /**
     * Masque les calques
     */
    VuePrincipale.prototype.masquerBandeaux = function () {
        $('#debut-partie').hide();
        $('#partie-gagnee').hide();
        $('#partie-perdue').hide();
    };
    /**
     * Actualise l'affichage du jeu
     */
    VuePrincipale.prototype.actualiser = function (jeu) {
        //Actualise le dessin du plateau de jeu
        jeu.dessiner(this.context);
        //Actualise les informations de partie
        this.actualiserNiveau(jeu.Niveau);
        this.actualiserScore(jeu.Score);
        this.actualiserPointsDeVie(jeu.PointsDeVie);
        this.actualiserMunitions(jeu.NombreMunitions);
    };
    /**
     * Actualise le numéro du niveau
     */
    VuePrincipale.prototype.actualiserNiveau = function (niveau) {
        $('#niveau').text('Niveau ' + (niveau + 1));
    };
    /**
     * Actualise le score
     */
    VuePrincipale.prototype.actualiserScore = function (score) {
        $('#score').text(score);
    };
    /**
     * Actualise les points de vie du joueur
     */
    VuePrincipale.prototype.actualiserPointsDeVie = function (score) {
        $('#points-de-vie').text(score);
    };
    /**
     * Actualise les munitions du joueur
     */
    VuePrincipale.prototype.actualiserMunitions = function (score) {
        $('#munitions').text(score);
    };
    return VuePrincipale;
}(Sujet));
//# sourceMappingURL=vue-principale.js.map