﻿/// <reference path="../patterns/sujet.ts" />
/// <reference path="../controlers/controleur-principal.ts" />
/// <reference path="../models/point.ts" />

/**
 * Vue principale de l'application
 */
class VuePrincipale extends Sujet
{
  private controleur: ControleurPrincipal;
  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  /**
   * Constructeur
   */
  constructor(controleur: ControleurPrincipal)
  {
	super();

	this.controleur = controleur;

	this.canvas = <HTMLCanvasElement>$('#canvas-dessin')[0];
	this.context = this.canvas.getContext('2d');

	this.attacherEvenements();
  }

  /**
   * Attache les événements aux éléments de la page Web
   */
  attacherEvenements()
  {
	$('#btnStart').bind('click', () => { this.controleur.commencerNouveauJeu(); });
	$('#btnReStart').bind('click', () => { this.controleur.commencerNouveauJeu(); });
	$('#btnNextLevel').bind('click', () => { this.controleur.commencerNiveauSuivant(); });

	$(this.canvas).bind('mousemove', (event) => {
	  var canvasPosition = $(this.canvas).offset();
	  var coordonnees = new Point(event.clientX - canvasPosition.left, event.clientY - canvasPosition.top + $(window).scrollTop());	  
	  this.controleur.onMouseMove(coordonnees);
	});

	$(this.canvas).bind('mousedown', () =>
	{
	  this.controleur.onMouseDown();
	});
  }

  /**
   * Retourne la largeur de la zone de dessin
   * @returns {number} La largeur de la zone de dessin
   */
  getLargeurDessin(): number
  {
	return this.canvas.width;
  }

  /**
   * Retourne la hauteur de la zone de dessin
   * @returns {number} La hauteur de la zone de dessin
   */
  getHauteurDessin(): number
  {
	return this.canvas.height;
  }

  /**
   * Redimensionne les éléments de la page en fonction de la taille de la fenêtre
   */
  redimensionner()
  {
	this.canvas.width = $(this.canvas).width();
	this.canvas.height = $(this.canvas).height();

	var marginTop = ($(window).height() - $('.bandeau').height()) / 2;
	$('.bandeau').css('margin-top', marginTop);
  }

  /**
   * Affiche le calque de début de partie
   */
  afficherDebutPartie()
  {
	$('#debut-partie').show();
  }

  /**
   * Affiche le calque de fin de partie (perdue)
   */
  afficherPartiePerdue()
  {
	$('#partie-perdue').show();
  }

  /**
   * Affiche le calque de fin de niveau (gagné)
   */
  afficherPartieGagnee()
  {
	$('#partie-gagnee').show();
  }

  /**
   * Masque les calques
   */
  masquerBandeaux()
  {
	$('#debut-partie').hide();
	$('#partie-gagnee').hide();
	$('#partie-perdue').hide();
  }

  /**
   * Actualise l'affichage du jeu
   */
  actualiser(jeu)
  {
	//Actualise le dessin du plateau de jeu
	jeu.dessiner(this.context);

	//Actualise les informations de partie
	this.actualiserNiveau(jeu.Niveau);
	this.actualiserScore(jeu.Score);
	this.actualiserPointsDeVie(jeu.PointsDeVie);
	this.actualiserMunitions(jeu.NombreMunitions);
  }

  /**
   * Actualise le numéro du niveau
   */
  actualiserNiveau(niveau)
  {
	$('#niveau').text('Niveau ' + (niveau + 1));
  }

  /**
   * Actualise le score
   */
  actualiserScore(score)
  {
	$('#score').text(score);
  }

  /**
   * Actualise les points de vie du joueur
   */
  actualiserPointsDeVie(score)
  {
	$('#points-de-vie').text(score);
  }

  /**
   * Actualise les munitions du joueur
   */
  actualiserMunitions(score)
  {
	$('#munitions').text(score);
  }
}