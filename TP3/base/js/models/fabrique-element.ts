﻿/// <reference path="humain.ts" />
/// <reference path="munition.ts" />
/// <reference path="pampmousse-mutant.ts" />
/// <reference path="presse-agrumes.ts" />
/// <reference path="../patterns/map.ts" />
/// <reference path="../libs/es6-promise.d.ts" />

/**
 * Fabrique des éléments graphiques du jeu
 */
class FabriqueElement
{
	
	/**
	 * Constructeur
	 */
	constructor()
	{
	  
	}

	/**
	 * Crée un élément graphique du type donné
	 */
	create(typeElement): ElementGraphique
	{
		var element: ElementGraphique;
		var textures = new Textures();

		if(typeElement == 'humain')
		{				
		  element = new Humain();
		}
		else if(typeElement == 'pampmousse mutant')
		{		
		  element = new PampmousseMutant();
		}
		else if(typeElement == 'munition')
		{		
		  element = new Munition();
		}
		else if(typeElement == 'presse agrumes')
		{		
		  element = new PresseAgrumes();
		}
		else
		{
		  throw "Type d'élément inconuu.";
		}

		return element;
	}
}