﻿/// <reference path="point.ts" />
/// <reference path="textures.ts" />

/**
 * Définit les propriétés et méthodes d'un élément graphique du jeu
 */
class ElementGraphique
{
  private coordonnees: Point;
  private rotation: number;
  private taille: number;
  private textures: Textures;
	/**
	 * Constructeur
	 */
	constructor()
	{
		//Coordonnées de l'élément
		this.coordonnees = new Point();

		//Angle de rotation (radians)
		this.rotation = 0;

		//Taille de l'élément (utilisée pour la détection des collision)
		this.taille = 0;

		//Collection de textures associées à l'élément
		this.textures = new Textures();
	}

	/**
	 * Setters
	 */
	public set Coordonnees(value: Point) { this.coordonnees.copier(value); }
	public set Rotation(value: number) { this.rotation = value; }
	public set Taille(value: number) { this.taille = value; }
	/**
	 * Getters
	 */
	public get Coordonnees(): Point { return this.coordonnees; }
	public get X(): number { return this.coordonnees.X; }
	public get Y():number { return this.coordonnees.Y; }
	public get Rotation(): number { return this.rotation; }
	public get Taille(): number { return this.taille; }
	public get Textures(): Textures { return this.textures; }
	
	/**
	 * Retourne le nombre de textures de l'élément
	 */
	getNombreTextures(): number { return this.textures.length(); }

	/**
	 * Ajoute une texture à l'élément graphique
	 * @param texture Texture à ajouter à l'élément graphique
	 */
	ajouterTexture(texture)
	{
		this.textures.add(texture);
	}

	/**
	 * Active la texture dont l'indice est donné en paramètre
	 * @param indiceTexture Indice de la texture à activer
	 */
	activerTexture(indiceTexture)
	{
		this.textures.activer(indiceTexture);
	}

	/**
	 * Retourne la texture active
	 */
	getTextureActive()
	{
		return this.textures.getTextureActive();
	}

	/**
	 * Retourne l'indice de la texture active
	 */
	getIndiceTextureActive()
	{
		return this.textures.getIndiceTextureActive();
	}

	/**
	 * Fonction de dessin de l'élément graphique
	 * @param context {Canvas2DContext} Context 2D du canvas sur lequel on souhaite dessiner l'élément graphique
	 */
	dessiner(context)
	{
		context.save();
		context.translate(this.X, this.Y);
		context.rotate(this.Rotation);

		var texture = this.getTextureActive();
		context.drawImage(texture, -texture.width / 2, -texture.height / 2);

		context.restore();
	}
}