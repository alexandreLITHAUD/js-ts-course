/// <reference path="../patterns/observateur.ts" />
/// <reference path="../patterns/sujet.ts" />
/// <reference path="fabrique-element.ts" />
/// <reference path="elements-graphiques.ts" />
/// <reference path="textures-loader.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Jeu "L'attaque des Pampmousses Mutants !"
 */
var Jeu = /** @class */ (function (_super) {
    __extends(Jeu, _super);
    /**
     * Constructeur
     */
    function Jeu(observateur) {
        var _this = _super.call(this) || this;
        _this.ajouterObservateur(observateur);
        //Fabrique permettant de générer les différents éléments graphiques du jeu
        _this.fabriqueElement = new FabriqueElement();
        //Liste des éléments graphiques présents sur le plateau
        _this.elementsGraphiques = new ElementsGraphiques();
        //Elément graphique symbolisant le joueur
        _this.joueur = null;
        //Indique que le niveau est terminé
        _this.termine = true;
        //Indique le niveau est gagné
        _this.gagne = false;
        //Indique le numéro du niveau en cours
        _this.niveau = 0;
        //Indique le score du joueur
        _this.score = 0;
        //Dimensions du plateau de jeu
        _this.largeurPlateau = 0;
        _this.hauteurPlateau = 0;
        //Texture du sol
        _this.textureSol = null;
        //Timer plaçant régulièrement des bonus sur le plateau
        _this.timerBonus = null;
        _this.initialiser();
        return _this;
    }
    Jeu.prototype.initialiser = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            TexturesLoader.chargerTextures().then(function () {
                _this.textureSol = TexturesLoader.getTexture("sol");
                resolve();
            }).catch(reject);
        });
    };
    Object.defineProperty(Jeu.prototype, "Niveau", {
        /**
         * Getters
         */
        get: function () { return this.niveau; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Jeu.prototype, "Scorce", {
        get: function () { return this.score; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Jeu.prototype, "NombreMunitions", {
        get: function () { return this.joueur.Munitions; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Jeu.prototype, "PointsDeVie", {
        get: function () { return this.joueur.PointsDeVie; },
        enumerable: false,
        configurable: true
    });
    /**
     * Définit les dimensions du plateau de jeu
     */
    Jeu.prototype.setDimensionsPlateau = function (largeur, hauteur) {
        this.largeurPlateau = largeur;
        this.hauteurPlateau = hauteur;
    };
    /**
     * Indique si la partie est terminée
     */
    Jeu.prototype.estTermine = function () {
        return this.termine;
    };
    /**
     * Indique si la joueur a gangé la manche
     */
    Jeu.prototype.estGagne = function () {
        return this.gagne;
    };
    /**
     * Démarre une nouvelle partie
     */
    Jeu.prototype.nouveau = function () {
        this.termine = false;
        this.gagne = false;
        this.score = 0;
        this.niveau = 0;
        this.demarrerNiveau();
    };
    /**
     * Passe au niveau suivant
     */
    Jeu.prototype.niveauSuivant = function () {
        this.termine = false;
        this.gagne = false;
        this.niveau++;
        this.demarrerNiveau();
    };
    /**
     * Démarre le niveau
     */
    Jeu.prototype.demarrerNiveau = function () {
        //Suppression des précédents éléments graphiques
        this.elementsGraphiques.clear();
        //Création du joueur
        this.joueur = this.fabriqueElement.create('humain');
        this.joueur.Coordonnees.X = this.largeurPlateau / 2;
        this.joueur.Coordonnees.Y = this.hauteurPlateau / 2;
        this.joueur.immuniser();
        this.elementsGraphiques.add(this.joueur);
        //Création des pampmousses mutants initiaux en fonction du niveau
        var nombrePampmousses = 5 + this.niveau;
        for (var iPampmousse = 0; iPampmousse < nombrePampmousses; ++iPampmousse) {
            this.ajouterPampmousseMutant();
        }
        this.demarrerDistributionBonus();
    };
    /**
     * Ajoute un élément graphique sur le plateau de jeu
     */
    Jeu.prototype.ajouterElement = function (element) {
        element.Coordonnees.copier(new Point(Math.floor(Math.random() * this.largeurPlateau), Math.floor(Math.random() * this.hauteurPlateau)));
        this.elementsGraphiques.add(element);
    };
    /**
     * Ajoute un pampmousse mutant sur le plateau de jeu
     */
    Jeu.prototype.ajouterPampmousseMutant = function () {
        // A faire : Question 1
        this.ajouterElement(this.fabriqueElement.create("pampmousse mutant"));
    };
    /**
     * Ajoute une munition sur le plateau de jeu
     */
    Jeu.prototype.ajouterMunition = function () {
        this.ajouterElement(this.fabriqueElement.create('munition'));
    };
    /**
     * Démarre la distribution aléatoire des munitions sur la plateau
     */
    Jeu.prototype.demarrerDistributionBonus = function () {
        var that = this;
        this.timerBonus = setTimeout(function () {
            that.distribuerBonus();
        }, Math.floor(Math.random() * 2000) + 3000);
    };
    /**
     * Ajoute une munition sur le plateau et prépare la prochaine distribution
     */
    Jeu.prototype.distribuerBonus = function () {
        this.ajouterMunition();
        this.demarrerDistributionBonus();
    };
    /**
     * Oriente le joueur vers les coordonnées données
     * @param coordonnees Coordonnées vers lequel on souhaite que le joueur se dirige
     */
    Jeu.prototype.orienterJoueurVers = function (coordonnees) {
        this.joueur.orienterVers(coordonnees);
    };
    /**
     * Génère un tire du joueur
     */
    Jeu.prototype.tirer = function () {
        //Si le joueur possède des munitions
        if (this.joueur.possedeMunitions()) {
            //On retire une munition au joueur
            this.joueur.tirer();
            //On place un presse-agrumes sur le plateau qui se déplace dans la même direction que le joueur
            var presseAgrumes = this.fabriqueElement.create('presse agrumes');
            presseAgrumes.Coordonnees = this.joueur.Coordonnees;
            presseAgrumes.Direction = this.joueur.Direction;
            this.elementsGraphiques.add(presseAgrumes);
        }
    };
    /**
     * Termine la partie
     */
    Jeu.prototype.terminer = function () {
        this.termine = true;
        clearTimeout(this.timerBonus);
        this.notifier();
    };
    /**
     * Anime les éléments du jeu
     */
    Jeu.prototype.animer = function () {
        this.elementsGraphiques.animer();
        this.gererCollissions();
        if (this.elementsGraphiques.getNombrePampmoussesMutants() == 0) {
            this.gagne = true;
            this.terminer();
        }
        this.notifier();
    };
    /**
     * Gère les éventuelles collisions entre les éléments présents sur le plateau de jeu
     */
    Jeu.prototype.gererCollissions = function () {
        //Pour chaque éléments du plateau de jeu
        for (var iElement = 0; iElement < this.elementsGraphiques.length(); ++iElement) {
            var element = this.elementsGraphiques.at(iElement);
            if (element instanceof ElementMobile) {
                //Teste la collision des éléments avec les murs
                if (this.testerCollisionsMurs(element)) {
                    //Si un pampmousse mutant touche un mur, il change de direction
                    if (element instanceof PampmousseMutant) {
                        element.Direction = Math.random() * 2 * Math.PI;
                    }
                    else if (element instanceof PresseAgrumes) {
                        //Si c'est un presse agrume, il est détruit
                        this.elementsGraphiques.remove(iElement);
                        --iElement;
                    }
                }
                //Teste les collision entre éléments
                for (var iElement2 = 0; iElement2 < this.elementsGraphiques.length(); ++iElement2) {
                    var element2 = this.elementsGraphiques.at(iElement2);
                    if (!(element2 instanceof PampmousseMutant) && !(element2 instanceof Munition))
                        continue;
                    //Si les deux éléments testés se touchent
                    if (element.touche(element2)) {
                        //Si un humain touche...
                        if (element instanceof Humain) {
                            //... une munition
                            if (element2 instanceof Munition) {
                                //La munition est retirée du jeu
                                this.elementsGraphiques.remove(iElement2);
                                if (iElement2 < iElement)
                                    iElement--;
                                --iElement2;
                                //Le joueur reçoit une munition
                                element.donnerMunition();
                            }
                            //... un pampmousse mutant
                            else if (element2 instanceof (PampmousseMutant)) {
                                this.joueur.blesser();
                                if (!this.joueur.estVivant()) {
                                    this.terminer();
                                }
                            }
                        }
                        // Si un presse-agrumes touche un pampmousse mutant
                        else if (element instanceof PresseAgrumes && element2 instanceof PampmousseMutant) {
                            this.elementsGraphiques.remove(iElement);
                            this.elementsGraphiques.remove(iElement2);
                            iElement--;
                            iElement2--;
                        }
                        //Si un pampmousse mutant touche une munition
                        else if (element instanceof PampmousseMutant && element2 instanceof Munition) {
                            this.elementsGraphiques.remove(iElement2);
                            iElement2--;
                            this.ajouterPampmousseMutant();
                            this.ajouterPampmousseMutant();
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Coordonnees.X = element.Coordonnees.X;
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Coordonnees.Y = element.Coordonnees.Y;
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Rotation = element.Direction * (120 * (Math.PI / 180));
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Coordonnees.X = element.Coordonnees.X;
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Coordonnees.Y = element.Coordonnees.Y;
                            this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Rotation = element.Direction * (360 * (Math.PI / 180));
                        }
                    }
                }
            }
        }
    };
    /**
     * Teste la collision d'un élément graphique du jeu avec les bords du plateau
     * @element Elément du jeu que l'on souhaite tester
     */
    Jeu.prototype.testerCollisionsMurs = function (element) {
        var valRet = false;
        var coordonnees = element.Coordonnees;
        //Test de collision avec les bords
        if (coordonnees.X > this.largeurPlateau || coordonnees.X < 0 || coordonnees.Y > this.hauteurPlateau || coordonnees.Y < 0) {
            if (coordonnees.X > this.largeurPlateau)
                coordonnees.X = this.largeurPlateau;
            else if (coordonnees.X < 0)
                coordonnees.X = 0;
            if (coordonnees.Y > this.hauteurPlateau)
                coordonnees.Y = this.hauteurPlateau;
            else if (coordonnees.Y < 0)
                coordonnees.Y = 0;
            valRet = true;
        }
        return valRet;
    };
    /**
     * Dessine le plateau de jeu
     */
    Jeu.prototype.dessiner = function (context) {
        this.dessinerSol(context);
        this.elementsGraphiques.dessiner(context);
    };
    /**
     * Dessine l'arrière plan du jeu
     */
    Jeu.prototype.dessinerSol = function (context) {
        var pattern = context.createPattern(this.textureSol, 'repeat');
        context.fillStyle = pattern;
        context.beginPath();
        context.rect(0, 0, this.largeurPlateau, this.hauteurPlateau);
        context.fill();
    };
    return Jeu;
}(Sujet));
//# sourceMappingURL=jeu.js.map