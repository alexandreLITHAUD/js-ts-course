﻿/// <reference path="textures-loader.ts" />
/// <reference path="element-graphique.ts" />

/**
 * Elément graphique représentant une munition
 */
class Munition extends ElementGraphique
{
	/**
	 * Constructeur
	 */
	constructor()
	{
		super();

		this.Taille = 10;
		this.ajouterTexture(TexturesLoader.getTexture("presse agrumes"));
	}
}