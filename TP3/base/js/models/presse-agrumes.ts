﻿/// <reference path="element-mobile.ts" />
/// <reference path="textures-loader.ts" />

/**
 * Presse agrume lancé par la joueur pour détruire les Pampmousses mutants
 */
class PresseAgrumes extends ElementMobile
{
	/**
	 * Constructeur
	 */
	constructor()
	{
		super();
	  
		this.ajouterTexture(TexturesLoader.getTexture("presse agrumes"));

		this.Vitesse = 10;
		this.Taille = 10;
	}

	/**
	 * Animation du presse agrume
	 */
	animer()
	{
		//A chaque appel le presse agrume tourne de 6 degres
		this.Rotation += Math.PI / 30;
		super.animer();
	}
}