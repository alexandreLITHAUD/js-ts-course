﻿"use strict";
/**
 * Représentation des coordonnées d'un point 2D
 */
class Point
{
  private x: number;
  private y: number;

  /**
   * Constructeur
   */
  constructor(x:number = 0, y:number = 0)
  {
	this.x = x;
	this.y = y;
  }

  /**
   * Setters
   */
  public set X(value: number) { this.x = value; }
  public set Y(value: number) { this.y = value; }

  /**
   * Getters
   */
  public get X(): number { return this.x; }
  public get Y(): number { return this.y; }

  /**
   * Copier le point donnée en paramètre
   * @param point Point dont on souhaite copier les données
   */
  copier(point)
  {
	this.X = point.X;
	this.Y = point.Y;
  }

  /**
   * Effectue une rotation du point par rapport au centre donné
   * @param center Coordonnées du centre de rotation
   * @param angleRadians Angre de rotation à effectuer
   */
  tourner(centre, angleRadians)
  {
	var x = this.X - centre.X;
	var y = this.Y - centre.Y;

	var newX = x * Math.cos(angleRadians) - y * Math.sin(angleRadians) + centre.X;
	var newY = x * Math.sin(angleRadians) + y * Math.cos(angleRadians) + centre.Y;

	this.X = newX; 
	this.Y = newY;
  }
}