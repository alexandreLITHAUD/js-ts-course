/// <reference path="element-mobile.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Elément mobile représentant un pampmousse mutant
 */
var PampmousseMutant = /** @class */ (function (_super) {
    __extends(PampmousseMutant, _super);
    /**
     * Constructeur
     */
    function PampmousseMutant() {
        var _this = _super.call(this) || this;
        _this.ajouterTexture(TexturesLoader.getTexture("pampmousse 1"));
        _this.ajouterTexture(TexturesLoader.getTexture("pampmousse 2"));
        _this.ajouterTexture(TexturesLoader.getTexture("pampmousse 3"));
        _this.ajouterTexture(TexturesLoader.getTexture("pampmousse 4"));
        setInterval(function () {
            var rand = Math.floor(Math.random() * 4);
            _this.activerTexture(rand);
        }, 750);
        setInterval(function () {
            _this.Vitesse++;
        }, 10000);
        return _this;
    }
    return PampmousseMutant;
}(ElementMobile));
//# sourceMappingURL=pampmousse-mutant.js.map