var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Extension d'un élément graphique lui permettant de se déplacer
 */
var ElementMobile = /** @class */ (function (_super) {
    __extends(ElementMobile, _super);
    /**
     * Constructeur
     */
    function ElementMobile() {
        var _this = _super.call(this) || this;
        //Direction de l'élément (en radians)
        _this.direction = Math.random() * 2 * Math.PI;
        //Vitesse de déplacement
        _this.vitesse = 3;
        return _this;
    }
    Object.defineProperty(ElementMobile.prototype, "Direction", {
        /**
         * Getters
         */
        get: function () { return this.direction; },
        /**
         * Setters
         */
        set: function (value) { this.direction = value; this.Rotation = value; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementMobile.prototype, "Vitesse", {
        get: function () { return this.vitesse; },
        set: function (value) { this.vitesse = value; },
        enumerable: false,
        configurable: true
    });
    /**
     * Indique si l'élément appelant touche l'élément fourni en paramètre
     * @param element Element avec lequel on souhaite tester une collision
     * @return Retourne true si une collision est détectée, false sinon
     */
    ElementMobile.prototype.touche = function (element) {
        return (this.X - this.Taille / 2 < element.X + element.Taille / 2 &&
            this.X + this.Taille / 2 > element.X - element.Taille / 2 &&
            this.Y - this.Taille / 2 < element.Y + element.Taille / 2 &&
            this.Y + this.Taille / 2 > element.Y - element.Taille / 2);
    };
    /**
     * Déplace l'élement à chaque appel
     */
    ElementMobile.prototype.animer = function () {
        //Déplacement selon la direction et la vitesse de l'élément
        var newCoordonnees = new Point();
        newCoordonnees.copier(this.Coordonnees);
        newCoordonnees.X = newCoordonnees.X + this.Vitesse;
        newCoordonnees.tourner(this.Coordonnees, this.Direction);
        this.Coordonnees.copier(newCoordonnees);
    };
    return ElementMobile;
}(ElementGraphique));
//# sourceMappingURL=element-mobile.js.map