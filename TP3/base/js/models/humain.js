/// <reference path="element-mobile.ts" />
/// <reference path="../libs/math.ts" />
/// <reference path="textures-loader.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Elément mobile représentant le joueur
 */
var Humain = /** @class */ (function (_super) {
    __extends(Humain, _super);
    /**
     * Constructeur
     */
    function Humain() {
        var _this = _super.call(this) || this;
        //Points de vie du joueur
        _this.pointsDeVie = 3;
        //Indique si le joueur est immunisé aux contacts des pampmousses mutants
        _this.immunise = false;
        //Nombre de munitions du joueur
        _this.munitions = 0;
        //Timer utilisé pour animer le clignotement du joueur
        _this.timerClignotement = null;
        //Timer utilisé pour gérer la durée d'immunisation du joueur
        _this.timerImmunisation = null;
        //Définit les textures à utiliser pour représenter le joueur
        _this.ajouterTexture(TexturesLoader.getTexture("humain"));
        _this.ajouterTexture(TexturesLoader.getTexture("humain immunise"));
        _this.activerTexture(0);
        //Définit la taille du joueur (pour détecter les collisions)
        _this.Taille = 20;
        //Définit la vitesse de déplacement du joueur
        _this.Vitesse = 5;
        return _this;
    }
    Object.defineProperty(Humain.prototype, "PointsDeVie", {
        /**
         * Retourne le nombre de points de vie restant au joueur
         */
        get: function () { return this.pointsDeVie; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Humain.prototype, "Munitions", {
        get: function () { return this.munitions; },
        enumerable: false,
        configurable: true
    });
    /**
     * Oriente le joueur en direction du point donné
     * @param coordonnees Coordonnées vers lesquelles on souhaite voir le joueur se déplacer
     */
    Humain.prototype.orienterVers = function (coordonnees) {
        var temp = new Point();
        temp.copier(this.Coordonnees);
        temp.X = temp.X + 10;
        this.Direction = Mathematiques.getAngle3Points(this.Coordonnees, temp, coordonnees) / 180 * Math.PI;
    };
    /**
     * Démarre la phase d'immunité du joueur
     */
    Humain.prototype.immuniser = function () {
        clearInterval(this.timerClignotement);
        clearTimeout(this.timerImmunisation);
        this.immunise = true;
        var that = this;
        this.timerClignotement = setInterval(function () { that.clignoter(); }, 100);
        this.timerImmunisation = setTimeout(function () { that.desimmuniser(); }, 3000);
    };
    /**
     * Termine la phase d'immunité du joueur
     */
    Humain.prototype.desimmuniser = function () {
        clearInterval(this.timerClignotement);
        clearTimeout(this.timerImmunisation);
        this.immunise = false;
        this.activerTexture(0);
    };
    /**
     * Fait clignoter le joueur (alterne les deux textures du joueur)
     */
    Humain.prototype.clignoter = function () {
        if (this.getIndiceTextureActive() == 0)
            this.activerTexture(1);
        else
            this.activerTexture(0);
    };
    /**
     * Blesse le joueur -> -1 point de vie, +déclenchement de l'immunité temporaire
     */
    Humain.prototype.blesser = function () {
        if (!this.estImmunise() && this.estVivant()) {
            this.pointsDeVie--;
            if (this.estVivant())
                this.immuniser();
        }
    };
    /**
     * Retire une munition au joueur
     */
    Humain.prototype.tirer = function () {
        if (this.possedeMunitions())
            this.munitions--;
    };
    /**
     * Ajoute une munition au joueur
     */
    Humain.prototype.donnerMunition = function () {
        this.munitions++;
    };
    /**
     * Indique si le joueur possède des munitions
     */
    Humain.prototype.possedeMunitions = function () {
        return this.munitions > 0;
    };
    /**
     * Indique si le joueur est vivant (lui reste-t-il des points de vie ?)
     */
    Humain.prototype.estVivant = function () {
        return this.pointsDeVie > 0;
    };
    /**
     * Indique si le joueur est invulnérable
     */
    Humain.prototype.estImmunise = function () {
        return this.immunise;
    };
    return Humain;
}(ElementMobile));
//# sourceMappingURL=humain.js.map