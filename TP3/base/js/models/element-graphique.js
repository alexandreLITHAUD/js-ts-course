/// <reference path="point.ts" />
/// <reference path="textures.ts" />
/**
 * Définit les propriétés et méthodes d'un élément graphique du jeu
 */
var ElementGraphique = /** @class */ (function () {
    /**
     * Constructeur
     */
    function ElementGraphique() {
        //Coordonnées de l'élément
        this.coordonnees = new Point();
        //Angle de rotation (radians)
        this.rotation = 0;
        //Taille de l'élément (utilisée pour la détection des collision)
        this.taille = 0;
        //Collection de textures associées à l'élément
        this.textures = new Textures();
    }
    Object.defineProperty(ElementGraphique.prototype, "Coordonnees", {
        /**
         * Getters
         */
        get: function () { return this.coordonnees; },
        /**
         * Setters
         */
        set: function (value) { this.coordonnees.copier(value); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementGraphique.prototype, "Rotation", {
        get: function () { return this.rotation; },
        set: function (value) { this.rotation = value; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementGraphique.prototype, "Taille", {
        get: function () { return this.taille; },
        set: function (value) { this.taille = value; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementGraphique.prototype, "X", {
        get: function () { return this.coordonnees.X; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementGraphique.prototype, "Y", {
        get: function () { return this.coordonnees.Y; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ElementGraphique.prototype, "Textures", {
        get: function () { return this.textures; },
        enumerable: false,
        configurable: true
    });
    /**
     * Retourne le nombre de textures de l'élément
     */
    ElementGraphique.prototype.getNombreTextures = function () { return this.textures.length(); };
    /**
     * Ajoute une texture à l'élément graphique
     * @param texture Texture à ajouter à l'élément graphique
     */
    ElementGraphique.prototype.ajouterTexture = function (texture) {
        this.textures.add(texture);
    };
    /**
     * Active la texture dont l'indice est donné en paramètre
     * @param indiceTexture Indice de la texture à activer
     */
    ElementGraphique.prototype.activerTexture = function (indiceTexture) {
        this.textures.activer(indiceTexture);
    };
    /**
     * Retourne la texture active
     */
    ElementGraphique.prototype.getTextureActive = function () {
        return this.textures.getTextureActive();
    };
    /**
     * Retourne l'indice de la texture active
     */
    ElementGraphique.prototype.getIndiceTextureActive = function () {
        return this.textures.getIndiceTextureActive();
    };
    /**
     * Fonction de dessin de l'élément graphique
     * @param context {Canvas2DContext} Context 2D du canvas sur lequel on souhaite dessiner l'élément graphique
     */
    ElementGraphique.prototype.dessiner = function (context) {
        context.save();
        context.translate(this.X, this.Y);
        context.rotate(this.Rotation);
        var texture = this.getTextureActive();
        context.drawImage(texture, -texture.width / 2, -texture.height / 2);
        context.restore();
    };
    return ElementGraphique;
}());
//# sourceMappingURL=element-graphique.js.map