/// <reference path="humain.ts" />
/// <reference path="munition.ts" />
/// <reference path="pampmousse-mutant.ts" />
/// <reference path="presse-agrumes.ts" />
/// <reference path="../patterns/map.ts" />
/// <reference path="../libs/es6-promise.d.ts" />
/**
 * Fabrique des éléments graphiques du jeu
 */
var FabriqueElement = /** @class */ (function () {
    /**
     * Constructeur
     */
    function FabriqueElement() {
    }
    /**
     * Crée un élément graphique du type donné
     */
    FabriqueElement.prototype.create = function (typeElement) {
        var element;
        var textures = new Textures();
        if (typeElement == 'humain') {
            element = new Humain();
        }
        else if (typeElement == 'pampmousse mutant') {
            element = new PampmousseMutant();
        }
        else if (typeElement == 'munition') {
            element = new Munition();
        }
        else if (typeElement == 'presse agrumes') {
            element = new PresseAgrumes();
        }
        else {
            throw "Type d'élément inconuu.";
        }
        return element;
    };
    return FabriqueElement;
}());
//# sourceMappingURL=fabrique-element.js.map