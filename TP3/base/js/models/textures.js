/// <reference path="../patterns/collection.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Gestionnaires de textures
 */
var Textures = /** @class */ (function (_super) {
    __extends(Textures, _super);
    /**
     * Constructeur
     */
    function Textures() {
        var _this = _super.call(this) || this;
        _this.indiceTextureActive = null;
        return _this;
    }
    /**
     * Ajoute une texture à la collection et définit cette texture comme active
     * @param element Texture à ajouter
     */
    Textures.prototype.add = function (element) {
        _super.prototype.add.call(this, element);
        this.activer(this.length() - 1);
    };
    /**
     * Active la texture dont l'indice est donné en paramètre
     * @param indiceTexture Indice de la texture à activer
     */
    Textures.prototype.activer = function (indiceTexture) {
        if (indiceTexture < 0 || indiceTexture >= this.length())
            throw "Textures::activer - Indice incorrect (" + indiceTexture + " / 0 - " + this.length() + ")";
        this.indiceTextureActive = indiceTexture;
    };
    /**
     * Retourne la texture actuellement active
     * @return Retourne la texture active
     * @throw Lève une exception si aucune texture n'est active
     */
    Textures.prototype.getTextureActive = function () {
        try {
            return this.at(this.indiceTextureActive);
        }
        catch (exception) {
            throw "Textures::getTextureActive - Texture active invalide.";
        }
    };
    /**
     * Retourne l'indice de la texture active
     * @return Retourne l'indice de la texture active
     */
    Textures.prototype.getIndiceTextureActive = function () {
        return this.indiceTextureActive;
    };
    return Textures;
}(Collection));
//# sourceMappingURL=textures.js.map