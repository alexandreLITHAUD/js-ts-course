/// <reference path="../patterns/collection.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Collection des éléments graphiques présents sur le jeu
 */
var ElementsGraphiques = /** @class */ (function (_super) {
    __extends(ElementsGraphiques, _super);
    /**
     * Constructeur
     */
    function ElementsGraphiques() {
        return _super.call(this) || this;
    }
    /**
     * Retourne le nombre de pampmousses mutants présents dans la liste
     */
    ElementsGraphiques.prototype.getNombrePampmoussesMutants = function () {
        var nbPampmousses = 0;
        for (var iElement = 0; iElement < this.length(); ++iElement) {
            if (this.at(iElement) instanceof PampmousseMutant)
                ++nbPampmousses;
        }
        return nbPampmousses;
    };
    /**
     * Anime l'ensemble des élements de la collection
     */
    ElementsGraphiques.prototype.animer = function () {
        for (var iElement = 0; iElement < this.length(); ++iElement) {
            var element = this.at(iElement);
            if (element instanceof ElementMobile) {
                element.animer();
            }
        }
    };
    /**
     * Dessine l'ensemble des élements de la collection
     * @param context {Canvas2DContext} Context 2D du canvas sur lequel on souhaite dessiner les éléments graphiques
     */
    ElementsGraphiques.prototype.dessiner = function (context) {
        for (var iElement = 0; iElement < this.length(); ++iElement) {
            var element = this.at(iElement);
            element.dessiner(context);
        }
    };
    return ElementsGraphiques;
}(Collection));
//# sourceMappingURL=elements-graphiques.js.map