﻿/// <reference path="../patterns/observateur.ts" />
/// <reference path="../patterns/sujet.ts" />
/// <reference path="fabrique-element.ts" />
/// <reference path="elements-graphiques.ts" />
/// <reference path="textures-loader.ts" />

/**
 * Jeu "L'attaque des Pampmousses Mutants !"
 */
class Jeu extends Sujet
{
  private fabriqueElement: FabriqueElement;
  private elementsGraphiques: ElementsGraphiques;
  private joueur: Humain;
  private termine: boolean;
  private gagne: boolean;
  private niveau: number;
  private score: number;
  private largeurPlateau: number;
  private hauteurPlateau: number;
  private timerBonus: number;
  private textureSol: HTMLImageElement;

  /**
   * Constructeur
   */
  constructor(observateur: Observateur)
  {
	super();
	this.ajouterObservateur(observateur);

	//Fabrique permettant de générer les différents éléments graphiques du jeu
	this.fabriqueElement = new FabriqueElement();

	//Liste des éléments graphiques présents sur le plateau
	this.elementsGraphiques = new ElementsGraphiques();

	//Elément graphique symbolisant le joueur
	this.joueur = null;

	//Indique que le niveau est terminé
	this.termine = true;

	//Indique le niveau est gagné
	this.gagne = false;

	//Indique le numéro du niveau en cours
	this.niveau = 0;

	//Indique le score du joueur
	this.score = 0;

	//Dimensions du plateau de jeu
	this.largeurPlateau = 0;
	this.hauteurPlateau = 0;

	//Texture du sol
	this.textureSol = null;

	//Timer plaçant régulièrement des bonus sur le plateau
	this.timerBonus = null;

	this.initialiser();
  }

  initialiser()
  {
	return new Promise((resolve, reject) => {
	  TexturesLoader.chargerTextures().then(() => {
		this.textureSol = TexturesLoader.getTexture("sol");
		resolve();		
	  }).catch(reject);
	});
  }

  /**
   * Getters
   */
  public get Niveau(): number { return this.niveau; }
  public get Scorce(): number { return this.score; }
  public get NombreMunitions(): number { return this.joueur.Munitions; }
  public get PointsDeVie(): number { return this.joueur.PointsDeVie; }

  /**
   * Définit les dimensions du plateau de jeu
   */
  setDimensionsPlateau(largeur: number, hauteur: number)
  {
	this.largeurPlateau = largeur;
	this.hauteurPlateau = hauteur;
  }

  /**
   * Indique si la partie est terminée
   */
  estTermine()
  {
	return this.termine;
  }

  /**
   * Indique si la joueur a gangé la manche
   */
  estGagne()
  {
	return this.gagne;
  }

  /**
   * Démarre une nouvelle partie
   */
  nouveau()
  {
	this.termine = false;
	this.gagne = false;
	this.score = 0;
	this.niveau = 0;

	this.demarrerNiveau();
  }

  /**
   * Passe au niveau suivant
   */
  niveauSuivant()
  {
	this.termine = false;
	this.gagne = false;
	this.niveau++;
	this.demarrerNiveau();
  }

  /**
   * Démarre le niveau
   */
  demarrerNiveau()
  {
	//Suppression des précédents éléments graphiques
	this.elementsGraphiques.clear();

	//Création du joueur
	this.joueur = <Humain>this.fabriqueElement.create('humain');
	this.joueur.Coordonnees.X = this.largeurPlateau / 2;
	this.joueur.Coordonnees.Y = this.hauteurPlateau / 2;
	this.joueur.immuniser();

	this.elementsGraphiques.add(this.joueur);

	//Création des pampmousses mutants initiaux en fonction du niveau
	var nombrePampmousses = 5 + this.niveau;

	for (var iPampmousse = 0; iPampmousse < nombrePampmousses; ++iPampmousse)
	{
	  this.ajouterPampmousseMutant();
	}

	this.demarrerDistributionBonus();
  }

  /**
   * Ajoute un élément graphique sur le plateau de jeu
   */
  ajouterElement(element)
  {
	element.Coordonnees.copier(new Point(Math.floor(Math.random() * this.largeurPlateau), Math.floor(Math.random() * this.hauteurPlateau)));

	this.elementsGraphiques.add(element);
  }

  /**
   * Ajoute un pampmousse mutant sur le plateau de jeu
   */
  ajouterPampmousseMutant()
  {
	  // A faire : Question 1
	  this.ajouterElement(this.fabriqueElement.create("pampmousse mutant"));
  }

  /**
   * Ajoute une munition sur le plateau de jeu
   */
  ajouterMunition()
  {
	this.ajouterElement(this.fabriqueElement.create('munition'));
  }

  /**
   * Démarre la distribution aléatoire des munitions sur la plateau
   */
  demarrerDistributionBonus()
  {
	var that = this;
	this.timerBonus = setTimeout(function ()
	{
	  that.distribuerBonus();
	}, Math.floor(Math.random() * 2000) + 3000);
  }

  /**
   * Ajoute une munition sur le plateau et prépare la prochaine distribution
   */
  distribuerBonus()
  {
	this.ajouterMunition();
	this.demarrerDistributionBonus();
  }

  /**
   * Oriente le joueur vers les coordonnées données
   * @param coordonnees Coordonnées vers lequel on souhaite que le joueur se dirige
   */
  orienterJoueurVers(coordonnees)
  {
	this.joueur.orienterVers(coordonnees);
  }

  /**
   * Génère un tire du joueur
   */
  tirer()
  {
	//Si le joueur possède des munitions
	if (this.joueur.possedeMunitions())
	{
	  //On retire une munition au joueur
	  this.joueur.tirer();

	  //On place un presse-agrumes sur le plateau qui se déplace dans la même direction que le joueur
	  var presseAgrumes: PresseAgrumes = <PresseAgrumes>this.fabriqueElement.create('presse agrumes');
	  presseAgrumes.Coordonnees = this.joueur.Coordonnees;
	  presseAgrumes.Direction = this.joueur.Direction;

	  this.elementsGraphiques.add(presseAgrumes);
	}
  }

  /**
   * Termine la partie
   */
  terminer()
  {
	this.termine = true;

	clearTimeout(this.timerBonus);

	this.notifier();
  }

  /**
   * Anime les éléments du jeu
   */
  animer()
  {
	this.elementsGraphiques.animer();

	this.gererCollissions();

	if (this.elementsGraphiques.getNombrePampmoussesMutants() == 0)
	{
	  this.gagne = true;
	  this.terminer();
	}

	this.notifier();
  }

  /**
   * Gère les éventuelles collisions entre les éléments présents sur le plateau de jeu
   */
  gererCollissions()
  {
	//Pour chaque éléments du plateau de jeu
	for (var iElement = 0; iElement < this.elementsGraphiques.length(); ++iElement)
	{
	  var element = this.elementsGraphiques.at(iElement);

	  if (element instanceof ElementMobile)
	  {
		//Teste la collision des éléments avec les murs
		if (this.testerCollisionsMurs(element))
		{
		  //Si un pampmousse mutant touche un mur, il change de direction
		  if (element instanceof PampmousseMutant)
		  {
			  element.Direction = Math.random() * 2*Math.PI;
		  }
		  else if (element instanceof PresseAgrumes)
		  {
			//Si c'est un presse agrume, il est détruit
			this.elementsGraphiques.remove(iElement);
			--iElement;
		  }
		}

		//Teste les collision entre éléments
		for (var iElement2 = 0; iElement2 < this.elementsGraphiques.length(); ++iElement2)
		{
		  var element2 = this.elementsGraphiques.at(iElement2);

		  if (!(element2 instanceof PampmousseMutant) && !(element2 instanceof Munition))
			continue;

		  //Si les deux éléments testés se touchent
		  if (element.touche(element2))
		  {
			//Si un humain touche...
			if (element instanceof Humain)
			{
			  //... une munition
			  if (element2 instanceof Munition)
			  {
				//La munition est retirée du jeu
				this.elementsGraphiques.remove(iElement2);

				if (iElement2 < iElement)
				  iElement--;

				--iElement2;

				//Le joueur reçoit une munition
				element.donnerMunition();
			  }
			  //... un pampmousse mutant
			  else if (element2 instanceof (PampmousseMutant))
			  {
				  this.joueur.blesser();
				  if (!this.joueur.estVivant()) {
					  this.terminer();
                  }
			  }
			}
			// Si un presse-agrumes touche un pampmousse mutant
			else if (element instanceof PresseAgrumes && element2 instanceof PampmousseMutant)
			{
				this.elementsGraphiques.remove(iElement);
				this.elementsGraphiques.remove(iElement2);
				iElement--;
				iElement2--;
			}
			//Si un pampmousse mutant touche une munition
			else if (element instanceof PampmousseMutant && element2 instanceof Munition)
			{
				this.elementsGraphiques.remove(iElement2);
				iElement2--;
				
				this.ajouterPampmousseMutant();
				this.ajouterPampmousseMutant();
				this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Coordonnees.X = element.Coordonnees.X;
				this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Coordonnees.Y = element.Coordonnees.Y;
				this.elementsGraphiques.at(this.elementsGraphiques.length() - 1).Rotation = element.Direction * (120 * (Math.PI / 180));

				this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Coordonnees.X = element.Coordonnees.X;
				this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Coordonnees.Y = element.Coordonnees.Y;
				this.elementsGraphiques.at(this.elementsGraphiques.length() - 2).Rotation = element.Direction * (360 * (Math.PI / 180));
			}
		  }
		}
	  }
	}
  }

  /**
   * Teste la collision d'un élément graphique du jeu avec les bords du plateau
   * @element Elément du jeu que l'on souhaite tester
   */
  testerCollisionsMurs(element)
  {
	var valRet = false;

	var coordonnees = element.Coordonnees;

	//Test de collision avec les bords
	if (coordonnees.X > this.largeurPlateau || coordonnees.X < 0 || coordonnees.Y > this.hauteurPlateau || coordonnees.Y < 0)
	{
	  if (coordonnees.X > this.largeurPlateau)
		coordonnees.X = this.largeurPlateau;
	  else if (coordonnees.X < 0)
		coordonnees.X = 0;

	  if (coordonnees.Y > this.hauteurPlateau)
		coordonnees.Y = this.hauteurPlateau;
	  else if (coordonnees.Y < 0)
		coordonnees.Y = 0;

	  valRet = true;
	}

	return valRet;
  }

  /**
   * Dessine le plateau de jeu
   */
  dessiner(context)
  {
	this.dessinerSol(context);
	this.elementsGraphiques.dessiner(context);
  }

  /**
   * Dessine l'arrière plan du jeu
   */
  dessinerSol(context)
  {
	var pattern = context.createPattern(this.textureSol, 'repeat');
	context.fillStyle = pattern;
	context.beginPath();
	context.rect(0, 0, this.largeurPlateau, this.hauteurPlateau);
	context.fill();
  }
}