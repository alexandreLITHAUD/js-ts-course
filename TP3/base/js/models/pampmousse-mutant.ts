﻿/// <reference path="element-mobile.ts" />

/**
 * Elément mobile représentant un pampmousse mutant
 */
class PampmousseMutant extends ElementMobile
{
	/**
	 * Constructeur
	 */
	constructor()
	{
		super();
		this.ajouterTexture(TexturesLoader.getTexture("pampmousse 1"));
		this.ajouterTexture(TexturesLoader.getTexture("pampmousse 2"));
		this.ajouterTexture(TexturesLoader.getTexture("pampmousse 3"));
		this.ajouterTexture(TexturesLoader.getTexture("pampmousse 4"));

		setInterval(() => {
			var rand = Math.floor(Math.random() * 4);
			this.activerTexture(rand);
		}, 750);

		setInterval(() => {
			this.Vitesse++;
		}, 10000);
	}
}