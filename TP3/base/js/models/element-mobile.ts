﻿/**
 * Extension d'un élément graphique lui permettant de se déplacer
 */
class ElementMobile extends ElementGraphique
{
  private direction: number;
  private vitesse: number;

  /**
   * Constructeur
   */
  constructor()
  {
	super();

	  //Direction de l'élément (en radians)
	  this.direction = Math.random() * 2 * Math.PI;

	//Vitesse de déplacement
	this.vitesse = 3;
  }

  /**
   * Setters
   */
  public set Direction(value: number) { this.direction = value; this.Rotation = value }
  public set Vitesse(value: number) { this.vitesse = value; }

  /**
   * Getters
   */
  public get Direction(): number { return this.direction; }
  public get Vitesse(): number { return this.vitesse; }

  /**
   * Indique si l'élément appelant touche l'élément fourni en paramètre
   * @param element Element avec lequel on souhaite tester une collision
   * @return Retourne true si une collision est détectée, false sinon
   */
  touche(element)
  {
	return (this.X - this.Taille / 2 < element.X + element.Taille / 2 &&
	  this.X + this.Taille / 2 > element.X - element.Taille / 2 &&
	  this.Y - this.Taille / 2 < element.Y + element.Taille / 2 &&
	  this.Y + this.Taille / 2 > element.Y - element.Taille / 2);
  }

  /**
   * Déplace l'élement à chaque appel
   */
  animer()
  {
	//Déplacement selon la direction et la vitesse de l'élément
	var newCoordonnees = new Point();
	newCoordonnees.copier(this.Coordonnees);
	newCoordonnees.X = newCoordonnees.X + this.Vitesse;
	newCoordonnees.tourner(this.Coordonnees, this.Direction);

	this.Coordonnees.copier(newCoordonnees);
  }
}