/// <reference path="element-mobile.ts" />
/// <reference path="textures-loader.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Presse agrume lancé par la joueur pour détruire les Pampmousses mutants
 */
var PresseAgrumes = /** @class */ (function (_super) {
    __extends(PresseAgrumes, _super);
    /**
     * Constructeur
     */
    function PresseAgrumes() {
        var _this = _super.call(this) || this;
        _this.ajouterTexture(TexturesLoader.getTexture("presse agrumes"));
        _this.Vitesse = 10;
        _this.Taille = 10;
        return _this;
    }
    /**
     * Animation du presse agrume
     */
    PresseAgrumes.prototype.animer = function () {
        //A chaque appel le presse agrume tourne de 6 degres
        this.Rotation += Math.PI / 30;
        _super.prototype.animer.call(this);
    };
    return PresseAgrumes;
}(ElementMobile));
//# sourceMappingURL=presse-agrumes.js.map