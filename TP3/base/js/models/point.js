"use strict";
/**
 * Représentation des coordonnées d'un point 2D
 */
var Point = /** @class */ (function () {
    /**
     * Constructeur
     */
    function Point(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.x = x;
        this.y = y;
    }
    Object.defineProperty(Point.prototype, "X", {
        /**
         * Getters
         */
        get: function () { return this.x; },
        /**
         * Setters
         */
        set: function (value) { this.x = value; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "Y", {
        get: function () { return this.y; },
        set: function (value) { this.y = value; },
        enumerable: false,
        configurable: true
    });
    /**
     * Copier le point donnée en paramètre
     * @param point Point dont on souhaite copier les données
     */
    Point.prototype.copier = function (point) {
        this.X = point.X;
        this.Y = point.Y;
    };
    /**
     * Effectue une rotation du point par rapport au centre donné
     * @param center Coordonnées du centre de rotation
     * @param angleRadians Angre de rotation à effectuer
     */
    Point.prototype.tourner = function (centre, angleRadians) {
        var x = this.X - centre.X;
        var y = this.Y - centre.Y;
        var newX = x * Math.cos(angleRadians) - y * Math.sin(angleRadians) + centre.X;
        var newY = x * Math.sin(angleRadians) + y * Math.cos(angleRadians) + centre.Y;
        this.X = newX;
        this.Y = newY;
    };
    return Point;
}());
//# sourceMappingURL=point.js.map