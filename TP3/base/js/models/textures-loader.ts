﻿class TexturesLoader
{
  static instance: TexturesLoader = null;

  private texturesFiles: Map<string>;
  private textures: Map<HTMLImageElement>;

  private constructor()
  {
	this.texturesFiles = {};
	this.textures = {};

	this.texturesFiles['humain'] = 'img/humain.png';
	this.texturesFiles['humain immunise'] = 'img/humain-immunise.png';

	this.texturesFiles['pampmousse 1'] = 'img/pampmousse1.png';
	this.texturesFiles['pampmousse 2'] = 'img/pampmousse2.png';
	this.texturesFiles['pampmousse 3'] = 'img/pampmousse3.png';
	this.texturesFiles['pampmousse 4'] = 'img/pampmousse4.png';

	this.texturesFiles['presse agrumes'] = 'img/presse-agrumes.png';

	this.texturesFiles['sol'] = 'img/sol.png';
  }

  static chargerTextures()
  {
	return new Promise((resolve, reject) =>
	{
	  var instance = TexturesLoader.getInstance();
	  var promesses = [];

	  Object.keys(instance.texturesFiles).forEach((key) =>
	  {
		promesses.push(new Promise((resolveFile, rejectFile) =>
		{
		  var texture = new Image();
		  texture.onload = () =>
		  {
			instance.textures[key] = texture;
			resolveFile();
		  };
		  texture.src = instance.texturesFiles[key];
		}));
	  });

	  Promise.all(promesses).then(resolve).catch(reject);
	});
  }

  static getTexture(nom: string): HTMLImageElement
  {
	var instance = TexturesLoader.getInstance();

	if(Object.keys(instance.textures).indexOf(nom) === -1)
	  throw "TexturesLoader::getTexture - Texture introuvable : '" + nom + "'";

	return instance.textures[nom];
  }

  static getInstance(): TexturesLoader
  {
	if (TexturesLoader.instance === null)
	  TexturesLoader.instance = new TexturesLoader();

	return TexturesLoader.instance;
  }
}