﻿/// <reference path="element-mobile.ts" />
/// <reference path="../libs/math.ts" />
/// <reference path="textures-loader.ts" />

/**
 * Elément mobile représentant le joueur
 */
class Humain extends ElementMobile
{
  private pointsDeVie: number;
  private immunise: boolean;
  private munitions: number;

  private timerClignotement: number;
  private timerImmunisation: number;
  /**
   * Constructeur
   */
  constructor()
  {
	super();

	//Points de vie du joueur
	this.pointsDeVie = 3;

	//Indique si le joueur est immunisé aux contacts des pampmousses mutants
	this.immunise = false;

	//Nombre de munitions du joueur
	this.munitions = 0;

	//Timer utilisé pour animer le clignotement du joueur
	this.timerClignotement = null;

	//Timer utilisé pour gérer la durée d'immunisation du joueur
	this.timerImmunisation = null;

	//Définit les textures à utiliser pour représenter le joueur
	this.ajouterTexture(TexturesLoader.getTexture("humain"));
	this.ajouterTexture(TexturesLoader.getTexture("humain immunise"));
	this.activerTexture(0);

	//Définit la taille du joueur (pour détecter les collisions)
	this.Taille = 20;

	//Définit la vitesse de déplacement du joueur
	this.Vitesse = 5;
  }

  /**
   * Retourne le nombre de points de vie restant au joueur
   */
  public get PointsDeVie(): number { return this.pointsDeVie; }
  public get Munitions(): number { return this.munitions; }
  
  /**
   * Oriente le joueur en direction du point donné
   * @param coordonnees Coordonnées vers lesquelles on souhaite voir le joueur se déplacer
   */
  orienterVers(coordonnees)
  {
	var temp = new Point();
	temp.copier(this.Coordonnees);
	temp.X = temp.X + 10;

	this.Direction = Mathematiques.getAngle3Points(this.Coordonnees, temp, coordonnees) / 180 * Math.PI;
  }

  /**
   * Démarre la phase d'immunité du joueur
   */
  immuniser()
  {
	clearInterval(this.timerClignotement);
	clearTimeout(this.timerImmunisation);

	this.immunise = true;

	var that = this;
	this.timerClignotement = setInterval(function () { that.clignoter(); }, 100);
	this.timerImmunisation = setTimeout(function () { that.desimmuniser(); }, 3000);
  }

  /**
   * Termine la phase d'immunité du joueur
   */
  desimmuniser()
  {
	clearInterval(this.timerClignotement);
	clearTimeout(this.timerImmunisation);

	this.immunise = false;

	this.activerTexture(0);
  }

  /**
   * Fait clignoter le joueur (alterne les deux textures du joueur)
   */
  clignoter()
  {
	if (this.getIndiceTextureActive() == 0)
	  this.activerTexture(1);
	else
	  this.activerTexture(0);
  }

  /**
   * Blesse le joueur -> -1 point de vie, +déclenchement de l'immunité temporaire
   */
  blesser()
  {
	if (!this.estImmunise() && this.estVivant())
	{
	  this.pointsDeVie--;

	  if (this.estVivant())
		this.immuniser();
	}

  }

  /**
   * Retire une munition au joueur
   */
  tirer()
  {
	if (this.possedeMunitions())
	  this.munitions--;
  }

  /**
   * Ajoute une munition au joueur
   */
  donnerMunition()
  {
	this.munitions++;
  }

  /**
   * Indique si le joueur possède des munitions
   */
  possedeMunitions()
  {
	return this.munitions > 0;
  }

  /**
   * Indique si le joueur est vivant (lui reste-t-il des points de vie ?)
   */
  estVivant()
  {
	return this.pointsDeVie > 0;
  }

  /**
   * Indique si le joueur est invulnérable
   */
  estImmunise()
  {
	return this.immunise;
  }
}