/// <reference path="../libs/jquery.d.ts" />
/// <reference path="../controlers/controleur-principal.ts" />
/**
 * Représente l'aplication Web
 */
var Application = /** @class */ (function () {
    //Constructeur
    function Application() {
        this.controleur = new ControleurPrincipal();
        this.controleur.initialiser();
    }
    return Application;
}());
//Démarrage de l'application une fois la page chargée
var application = null;
$(window).ready(function () { application = new Application(); });
//# sourceMappingURL=application.js.map