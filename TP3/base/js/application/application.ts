﻿/// <reference path="../libs/jquery.d.ts" />
/// <reference path="../controlers/controleur-principal.ts" />

/**
 * Représente l'aplication Web
 */
class Application
{
  //Constroleur de l'application
  private controleur: ControleurPrincipal;

  //Constructeur
  constructor()
  {
	this.controleur = new ControleurPrincipal();
	this.controleur.initialiser();
  }  
}

//Démarrage de l'application une fois la page chargée
var application: Application = null;
$(window).ready(() => { application = new Application(); });