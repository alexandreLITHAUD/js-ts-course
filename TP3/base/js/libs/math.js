var Mathematiques = /** @class */ (function () {
    function Mathematiques() {
    }
    /**
     * Retourne l'angle en degres formé par trois points
     * @param centre Centre de l'angle à déterminé
     * @param debut Première extrémité de l'angle
     * @param fin Deuxième extréité de l'angle
     */
    Mathematiques.getAngle3Points = function (centre, debut, fin) {
        if (debut.X == centre.X && debut.Y == centre.Y || fin.X == centre.X && fin.Y == centre.Y)
            return 0.0;
        var angleDebut = Mathematiques.getAngle(centre, debut);
        var angleFin = Mathematiques.getAngle(centre, fin);
        var valRet = angleFin - angleDebut;
        if (valRet < 0)
            valRet += 360;
        return valRet;
    };
    /**
     * Retourne l'angle en degres formé par l'axe horizontal pasant par (xOrigine ; yOrigine) et le point (xPoint ; yPoint)
     * @param origine Point d'origine par rapport auquel mesuré l'angle formé
     * @param point Poit format l'angle à mesurer avec l'axe horizontal passant par origine
     */
    Mathematiques.getAngle = function (origine, point) {
        var valRet = 0;
        var rayon = Math.sqrt(Math.pow(origine.X - point.X, 2) + Math.pow(origine.Y - point.Y, 2));
        valRet = (Math.asin(Math.abs(point.Y - origine.Y) / rayon)) / Math.PI * 180;
        if (point.X < origine.X)
            valRet = 180 - valRet;
        if (point.Y < origine.Y)
            valRet = 360 - valRet;
        return valRet;
    };
    return Mathematiques;
}());
//# sourceMappingURL=math.js.map