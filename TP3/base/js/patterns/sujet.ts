﻿/**
 * Classe Sujet (Pattern Sujet / Observateur)
 */
class Sujet
{
  //Les des observateurs du sujet
  private observateurs: Array<Observateur>;
  /**
   * Constructeur
   */
  constructor()
  {
	this.observateurs = new Array<Observateur>();
  }

  /**
   * Ajoute un observateur au sujet
   * @param observateur {Observateur} Observateur à ajouter au sujet
   */
  ajouterObservateur(observateur)
  {
	this.observateurs.push(observateur);
  }

  /**
   * Notifie les observateurs
   */
  notifier()
  {
	for (var iObservateur in this.observateurs)
	  this.observateurs[iObservateur].notifier();
  }
}