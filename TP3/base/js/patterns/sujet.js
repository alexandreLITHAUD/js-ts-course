/**
 * Classe Sujet (Pattern Sujet / Observateur)
 */
var Sujet = /** @class */ (function () {
    /**
     * Constructeur
     */
    function Sujet() {
        this.observateurs = new Array();
    }
    /**
     * Ajoute un observateur au sujet
     * @param observateur {Observateur} Observateur à ajouter au sujet
     */
    Sujet.prototype.ajouterObservateur = function (observateur) {
        this.observateurs.push(observateur);
    };
    /**
     * Notifie les observateurs
     */
    Sujet.prototype.notifier = function () {
        for (var iObservateur in this.observateurs)
            this.observateurs[iObservateur].notifier();
    };
    return Sujet;
}());
//# sourceMappingURL=sujet.js.map