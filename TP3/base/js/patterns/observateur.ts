﻿/**
 * Interface observateur (Pattern Sujet / Observateur)
 */
interface Observateur
{
  //Notifie l'observateur
  notifier();
}