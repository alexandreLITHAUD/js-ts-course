﻿
class View extends Observer{

    /**
     * Init the View
     * @param {Controller} controllerr
     */
    constructor(controllerr) {
        super();
        this.controller = controllerr;
        this.controller.addObserver(this);

        $("#btn-add").on("click", () => { this.clicAdd() });

        $("#btn-update").on("click", () => { this.controller.updateGame(this.getSelectionIndex(), this.createGame()) });

        $("#btn-remove").on("click", () => { this.controller.removeGame(this.getSelectionIndex())});
    }

    notify() {
        console.log(this.controller);
        this.refreshList();
    }

    /**
     * @returns {Game} the new game
     * */
    createGame() {
        let game = new Game();
        let title = $("#txt-title").val();
        let release = $("#txt-release-year").val();
        game.setTitle(title);
        game.setReleaseYear(release);

        return game;
    }

    clicAdd() {
        let game = this.createGame();
        this.controller.addGame(game);
    }

    refreshList() {

        let games = this.controller.getGames();

        $("content list").empty();

        games.forEach((game) => {

            let newDiv = document.createElement("div");
            newDiv.classList.add("game");

            let pTitle = document.createElement("p");
            pTitle.classList.add("title");
            pTitle.innerText = game.getTitle();
            newDiv.appendChild(pTitle);

            let pRealease = document.createElement("p");
            pRealease.classList.add("info");
            pRealease.innerText = game.getReleaseYear();
            newDiv.appendChild(pRealease);

            newDiv.addEventListener("click", () => {
                this.displayGame(game);
                $(".game").removeClass("selected");
                newDiv.classList.add("selected");
            });

            $("content list").append(newDiv);
        })

    }

    /**
     * 
     * @param {Game} game
     */
    displayGame(game) {

        $("#txt-title").val(game.getTitle());
        $("#txt-release-year").val(game.getReleaseYear());

    }

    /**
     * @returns {number} index du jeu selectionné
     * */
    getSelectionIndex() {
        return $(".selected").index();
    }

}