﻿/**
 * Web application
 */
class Application
{
    /**
     * Constructor
     */
    constructor()
    {
        this.games = new Games();
        this.controller = new Controller(this.games);
        this.view = new View(this.controller);  
    }
}


/**
 * Start the application after the loading of the page is complete
 */
window.onload = function ()
{
    let application = new Application();
};