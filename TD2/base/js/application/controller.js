﻿
class Controller extends Subject{

    /**
     * Init the Controller
     * @param {Games} games
     */
    constructor(games) {
        super();
        this.games = games;
    }

    /**
     * Add a game to the controller
     * @param {Game} game
     */
    addGame(game) {
        this.games.add(game);
        //this.observers.forEach((cont) => { cont.notify() });
        this.notify();
    }

    getGames() { return this.games; }

    /**
     * 
     * 
     * @param {number} index
     * @returns {Game} game
     */
    getGame(index) {

        return this.games.at(index);

    }

    /**
     * 
     * @param {number} index
     * @param {Game} game
     */
    updateGame(index, game) {
        this.games.replace(index, game);
        this.notify();
    }

    /**
     * 
     * @param {number} index
     */
    removeGame(index) {
        this.games.remove(index);
        this.notify();
    }


}