﻿
/**
 * Classe qui gère un slider
 * */
class sliderPoo {

    /**
     * Constructeur de la classe sliderPOO
     * */
    constructor() {
        this.current = 0;
        this.timer = 0;

        window.onload = () => {

            this.affiche(this.current);

            //EventListener des boutons 
            $(".slider-control").on("click", () => {
                let pos = $(".slider-control").index($(event.target));
                this.affiche(pos);
                this.current = pos;
            });

            //EventListener de la checkbox
            $("#auto").on("click", () => {
                if ($("#auto").prop("checked")) {
                    this.startTimer();
                } else {
                    this.stopTimer();
                }
            });

        }
    }

    /**
     * Fonction qui affiche l'image d'après
     * */
    nextImage() {
        this.current = (this.current + 1) % 4;
        this.affiche(this.current);
    }

    /**
     * Fonction qui lance le timer
     * */
    startTimer() {
        this.timer = setInterval(() => {
            this.nextImage();
        }, 2000);
    }

    /**
     * Fonction qui arrete le timer
     * */
    stopTimer() {
        clearInterval(this.timer);
    }

    /**
     * Fonction qui affiche l'image du numero en paramètre
     * @param {number} number
     */
    affiche(number) {
        //images
        $(".slider-panel").removeClass("active");
        $(".slider-panel").eq(number).addClass("active");

        //bouton
        $(".slider-control").removeClass("active");
        $(".slider-control").eq(number).addClass("active");
    }

}
//""""""Main""""""
var main = new sliderPoo();