﻿/// <reference path="observer.ts" />
/***
 * Observable subject
 */
class Subject
{
    private observers: Array<Observer>;

	/**
	 * Constructor
	 */
	constructor()
	{
		this.observers = new Array<Observer>();
	}

	/**
	 * Add an observer
	 * @param observer: Observer to add
	 */
	addObserver(observer: Observer)
	{
        this.observers.push(observer);
	}

	/**
	 * Notify each observer
	 */
	notify()
	{
        this.observers.forEach(function (observer)
		{
            observer.notify();
		});
	}
}