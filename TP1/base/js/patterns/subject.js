/// <reference path="observer.ts" />
/***
 * Observable subject
 */
var Subject = /** @class */ (function () {
    /**
     * Constructor
     */
    function Subject() {
        this.observers = new Array();
    }
    /**
     * Add an observer
     * @param observer: Observer to add
     */
    Subject.prototype.addObserver = function (observer) {
        this.observers.push(observer);
    };
    /**
     * Notify each observer
     */
    Subject.prototype.notify = function () {
        this.observers.forEach(function (observer) {
            observer.notify();
        });
    };
    return Subject;
}());
//# sourceMappingURL=subject.js.map