﻿/**
 * Observer of subjects
 */
interface Observer
{	
	/**
	 * Notify function has to be overridden
	 */
    notify();
}