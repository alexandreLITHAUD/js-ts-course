﻿/// <reference path="../../definitely-typed/three.d.ts" />
/// <reference path="../modeles/string-map.ts" />
/// <reference path="../../definitely-typed/jquery.d.ts" />
/// <reference path="controller.ts" />
/**
 * Vue principale de l'application
 */
class View implements Observer
{
    /**
     * Main controller
     */
    private stringMap: StringMap<THREE.Object3D>;

    private _controller: Controller;
    private renderer: THREE.WebGLRenderer;
    private scene: THREE.Scene;
    private camera: THREE.PerspectiveCamera;

    /**
     * Constructor
     * @param controller Main controller
     */
    constructor(controller: Controller)
    {
        this.stringMap = new StringMap();

        this._controller = controller;
        this._controller.addObserver(this);
        this.renderer = new THREE.WebGLRenderer({ canvas: this.getCanvas(), antialias:true });
        this.renderer.shadowMapEnabled = true;
        this.scene = new THREE.Scene();
        let canva = this.getCanvas();
        this.camera = new THREE.PerspectiveCamera(60, canva.width / canva.height, 1, 10000);

        this.addCube();
        this.addAmbientLight();
        this.addSpotLight();
        this.addFloor();

        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = 30;

        canva.addEventListener("wheel", (event: WheelEvent) => {
            this.camera.position.z += event.deltaY;
            event.preventDefault();
        });

        canva.addEventListener("mousemove", (event: MouseEvent) => {
            if (event.ctrlKey) {
                this.camera.position.x += -event.movementX / 50;
                this.camera.position.y += event.movementY / 50;
            }
            event.preventDefault();
        });
        
        this.render();
    }

    /**
     * Notify function
     */
    notify()
    {

    }

    /**
     * Return the canvas of the web page
     */
    getCanvas(): HTMLCanvasElement
    {
        return <HTMLCanvasElement>$('canvas')[0];
    }

    /**
     * Render the image in the canvas
     * */
    render() {

        this.stringMap["cube"].rotateY(0.01);
        this.renderer.render(this.scene, this.camera);

        requestAnimationFrame(() => { this.render() }); 
    }

    /**
     * Create the Cube
     * */
    addCube() {
        let loader = new THREE.TextureLoader();

        let texture = loader.load("img/crate.jpg");
        texture.anisotropy = 16;
        let textureBumpMap = loader.load("img/crate-relief.jpg");
        textureBumpMap.anisotropy = 16;

        let matos = new THREE.MeshPhongMaterial({ map: texture });
        matos.bumpMap = textureBumpMap;
        matos.bumpScale = 0.5;

        let geom = new THREE.BoxGeometry(10, 10, 10);

        let obj = new THREE.Mesh(geom, matos); 

        obj.position.x = 0;
        obj.position.y = 0;
        obj.position.z = 0;
 
        //obj.rotation.y = 0.5;

        obj.castShadow = true;
        this.stringMap["cube"] = obj;
        this.scene.add(obj);
    }

    /**
     * Create the Floor
     * */
    addFloor() {
        let loader = new THREE.TextureLoader();

        let texture = loader.load("img/floor.jpg");
        let textureBumpMap = loader.load("img/floor-relief.jpg");
        texture.anisotropy = 16;
        textureBumpMap.anisotropy = 16;

        let geom = new THREE.BoxGeometry(40, 1, 40);

        let matos = new THREE.MeshPhongMaterial({ map: texture });
        matos.bumpMap = textureBumpMap;
        matos.bumpScale = 0.5;

        let floor = new THREE.Mesh(geom, matos);

        floor.position.x = 0;
        floor.position.y = -7;
        floor.position.z = 0;

        floor.receiveShadow = true;
        this.stringMap["floor"] = floor;
        this.scene.add(floor);
    }

    /**
     * Create the Ambiant light
     * */
    addAmbientLight() {

        let light = new THREE.AmbientLight(0xffffff, 0.3);

        //light.castShadow = true;
        this.stringMap["ambientLight"] = light;
        this.scene.add(light);
        
    }

    /**
     * Create the SpotLight
     * */
    addSpotLight() {
        let spot = new THREE.SpotLight(0xffffff, 1);

        spot.position.x = 20;
        spot.position.y = 20;
        spot.position.z = 20;

        spot.castShadow = true;
        this.stringMap["spotLight"] = spot;
        this.scene.add(spot);
    }
}