﻿/// <reference path="../patterns/subject.ts" />

/**
 * Controleur principal de l'application
 */
class Controller extends Subject
{
    /**
     * Constructor
     */
    constructor()
    {
        super();
    }
}