/// <reference path="../../definitely-typed/jquery.d.ts" />
/// <reference path="controller.ts" />
/// <reference path="view.ts" />
/**
 * Web application
 */
var Application = /** @class */ (function () {
    /**
     * Constructor
     */
    function Application() {
        this._controller = new Controller();
        this._view = new View(this._controller);
    }
    return Application;
}());
/**
 * Démarrage de l'application Web une fois la page chargée.
 */
var application = null;
$(window).ready(function () { application = new Application(); });
//# sourceMappingURL=application.js.map