﻿/// <reference path="../../definitely-typed/jquery.d.ts" />
/// <reference path="controller.ts" />
/// <reference path="view.ts" />

/**
 * Web application
 */
class Application
{
    /**
     * Main controller
     */
    private _controller: Controller;

    /**
     * Main view
     */
    private _view: View;

    /**
     * Constructor
     */
    constructor()
    {
        this._controller = new Controller();
        this._view = new View(this._controller);
    }
}

/**
 * Démarrage de l'application Web une fois la page chargée.
 */
var application: Application = null;
$(window).ready(() => { application = new Application(); });