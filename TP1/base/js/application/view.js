/// <reference path="../../definitely-typed/three.d.ts" />
/// <reference path="../modeles/string-map.ts" />
/// <reference path="../../definitely-typed/jquery.d.ts" />
/// <reference path="controller.ts" />
/**
 * Vue principale de l'application
 */
var View = /** @class */ (function () {
    /**
     * Constructor
     * @param controller Main controller
     */
    function View(controller) {
        var _this = this;
        this.stringMap = new StringMap();
        this._controller = controller;
        this._controller.addObserver(this);
        this.renderer = new THREE.WebGLRenderer({ canvas: this.getCanvas(), antialias: true });
        this.renderer.shadowMapEnabled = true;
        this.scene = new THREE.Scene();
        var canva = this.getCanvas();
        this.camera = new THREE.PerspectiveCamera(60, canva.width / canva.height, 1, 10000);
        this.addCube();
        this.addAmbientLight();
        this.addSpotLight();
        this.addFloor();
        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = 30;
        canva.addEventListener("wheel", function (event) {
            _this.camera.position.z += event.deltaY;
            event.preventDefault();
        });
        canva.addEventListener("mousemove", function (event) {
            if (event.ctrlKey) {
                _this.camera.position.x += -event.movementX / 50;
                _this.camera.position.y += event.movementY / 50;
            }
            event.preventDefault();
        });
        this.render();
    }
    /**
     * Notify function
     */
    View.prototype.notify = function () {
    };
    /**
     * Return the canvas of the web page
     */
    View.prototype.getCanvas = function () {
        return $('canvas')[0];
    };
    /**
     * Render the image in the canvas
     * */
    View.prototype.render = function () {
        var _this = this;
        this.stringMap["cube"].rotateY(0.01);
        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(function () { _this.render(); });
    };
    /**
     * Create the Cube
     * */
    View.prototype.addCube = function () {
        var loader = new THREE.TextureLoader();
        var texture = loader.load("img/crate.jpg");
        texture.anisotropy = 16;
        var textureBumpMap = loader.load("img/crate-relief.jpg");
        textureBumpMap.anisotropy = 16;
        var matos = new THREE.MeshPhongMaterial({ map: texture });
        matos.bumpMap = textureBumpMap;
        matos.bumpScale = 0.5;
        var geom = new THREE.BoxGeometry(10, 10, 10);
        var obj = new THREE.Mesh(geom, matos);
        obj.position.x = 0;
        obj.position.y = 0;
        obj.position.z = 0;
        //obj.rotation.y = 0.5;
        obj.castShadow = true;
        this.stringMap["cube"] = obj;
        this.scene.add(obj);
    };
    /**
     * Create the Floor
     * */
    View.prototype.addFloor = function () {
        var loader = new THREE.TextureLoader();
        var texture = loader.load("img/floor.jpg");
        var textureBumpMap = loader.load("img/floor-relief.jpg");
        texture.anisotropy = 16;
        textureBumpMap.anisotropy = 16;
        var geom = new THREE.BoxGeometry(40, 1, 40);
        var matos = new THREE.MeshPhongMaterial({ map: texture });
        matos.bumpMap = textureBumpMap;
        matos.bumpScale = 0.5;
        var floor = new THREE.Mesh(geom, matos);
        floor.position.x = 0;
        floor.position.y = -7;
        floor.position.z = 0;
        floor.receiveShadow = true;
        this.stringMap["floor"] = floor;
        this.scene.add(floor);
    };
    /**
     * Create the Ambiant light
     * */
    View.prototype.addAmbientLight = function () {
        var light = new THREE.AmbientLight(0xffffff, 0.3);
        //light.castShadow = true;
        this.stringMap["ambientLight"] = light;
        this.scene.add(light);
    };
    /**
     * Create the SpotLight
     * */
    View.prototype.addSpotLight = function () {
        var spot = new THREE.SpotLight(0xffffff, 1);
        spot.position.x = 20;
        spot.position.y = 20;
        spot.position.z = 20;
        spot.castShadow = true;
        this.stringMap["spotLight"] = spot;
        this.scene.add(spot);
    };
    return View;
}());
//# sourceMappingURL=view.js.map