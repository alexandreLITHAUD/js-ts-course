﻿/**
 * Map string => T
 */
class StringMap<T>
{
  [key: string]: T;
}