/// <reference path="reservation.ts" />
var Vue = /** @class */ (function () {
    function Vue() {
        var _this = this;
        console.log("application crée");
        var r = new Reservation();
        this.chargerReservation(r);
        console.log(r);
        this.afficherReservation(r);
        $("form").accordion();
        $("#txt-adults").spinner({
            min: 0
        });
        $("#txt-children").spinner({
            min: 0
        });
        $("input[type=checkbox]").checkboxradio();
        $(".activity").draggable({
            helper: "clone",
            revert: "invalid"
        });
        $("div.activities").droppable({
            accept: ".activity",
            drop: function (event, ui) {
                ui.draggable.appendTo(event.target);
            }
        });
        $("#btn-validate").on("click", function () {
            var r = _this.lireReservation();
            console.log(r);
            _this.afficherResume(r);
            _this.sauverReservation(r);
        });
    }
    Vue.prototype.lireReservation = function () {
        var res = new Reservation();
        res.Arrivee = $("#txt-arrival").val();
        res.Depart = $("#txt-departure").val();
        res.NbAdultes = $("#txt-adults").val();
        res.NbEnfants = $("#txt-children").val();
        res.PetitDej = $("#chk-breakfast").prop("checked");
        res.Dejeuner = $("#chk-lunch").prop("checked");
        res.Diner = $("#chk-dinner").prop("checked");
        $("#desired-activities .activity").each(function (index, item) {
            res.ajouterActivite($(item).attr("title"));
        });
        return res;
    };
    Vue.prototype.afficherResume = function (r) {
        var summary = $("#summary");
        summary.append("<h1>" + 'Resumé' + "</h1>");
        summary.append("<h2>" + 'Dates' + "</h2>");
        summary.append("<ul>");
        summary.append("<li>" + "Arrivée : " + r.Arrivee + "</li>");
        summary.append("<li>" + "Départ : " + r.Depart + "</li>");
        summary.append("</ul>");
        summary.append("<h2>" + 'Personnes' + "</h2>");
        summary.append("<p>" + r.NbAdultes + " adultes " + r.NbEnfants + " enfants" + "</p>");
        summary.append("<h2>" + 'Repas' + "</h2>");
        summary.append("<ul>");
        if (r.PetitDej) {
            summary.append("<li>" + "Petit dejeuner" + "</li>");
        }
        if (r.Dejeuner) {
            summary.append("<li>" + "Dejeuner" + "</li>");
        }
        if (r.Diner) {
            summary.append("<li>" + "Diner" + "</li>");
        }
        summary.append("</ul>");
        summary.append("<h2>" + 'Activités choisies' + "</h2>");
        summary.append("<ul>");
        r.ActivitesChoisies.forEach(function (item) {
            summary.append("<li>" + item + "</li>");
        });
        summary.append("</ul>");
    };
    Vue.prototype.afficherReservation = function (r) {
        $("#txt-arrival").val(r.Arrivee);
        $("#txt-departure").val(r.Depart);
        $("#txt-adults").val(r.NbAdultes);
        $("#txt-children").val(r.NbEnfants);
        $("#chk-breakfast").prop("checked", r.PetitDej);
        $("#chk-lunch").prop("checked", r.Dejeuner);
        $("#chk-dinner").prop("checked", r.Diner);
        // Ne marche pas finalement
        /*        $(".activities .activity").each((index, item) => {
                    let oui = false;
                    let temp = $(item).attr("title");
                    console.log(temp);
        
                    r.ActivitesChoisies.forEach((index, item) => {
                        if (r.ActivitesChoisies[index] == temp) {
                            oui = true;
                        }
                    });
        
                    if (oui) {
                        oui = false;
                        $("#desired-activities .activity").append(item);
                    } else {
                        
                        $("#rejected-activities .activity").append(item);
                    }
        
                });*/
    };
    Vue.prototype.sauverReservation = function (r) {
        window.localStorage.setItem("RESERVATION", JSON.stringify(r));
    };
    Vue.prototype.chargerReservation = function (reserv) {
        var str = window.localStorage.getItem("RESERVATION");
        if (str) {
            var tab = JSON.parse(str);
            reserv.copier(tab);
        }
        return reserv;
    };
    return Vue;
}());
//# sourceMappingURL=Vue.js.map