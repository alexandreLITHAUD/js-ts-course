var Reservation = /** @class */ (function () {
    function Reservation() {
        this.activitesChoisies = new Array();
    }
    Object.defineProperty(Reservation.prototype, "Arrivee", {
        get: function () { return this.arrivee; },
        set: function (value) {
            this.arrivee = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "Depart", {
        get: function () { return this.depart; },
        set: function (value) {
            this.depart = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "NbAdultes", {
        get: function () { return this.nbAdultes; },
        set: function (value) {
            this.nbAdultes = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "NbEnfants", {
        get: function () { return this.nbEnfants; },
        set: function (value) {
            this.nbEnfants = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "PetitDej", {
        get: function () { return this.petitDej; },
        set: function (value) {
            this.petitDej = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "Dejeuner", {
        get: function () { return this.dejeuner; },
        set: function (value) {
            this.dejeuner = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "Diner", {
        get: function () { return this.diner; },
        set: function (value) {
            this.diner = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Reservation.prototype, "ActivitesChoisies", {
        get: function () { return this.activitesChoisies; },
        enumerable: false,
        configurable: true
    });
    Reservation.prototype.ajouterActivite = function (s) {
        this.activitesChoisies.push(s);
    };
    Reservation.prototype.copier = function (param) {
        var _this = this;
        this.arrivee = param.arrivee;
        this.depart = param.depart;
        this.dejeuner = param.dejeuner;
        this.petitDej = param.petitDej;
        this.diner = param.diner;
        this.nbEnfants = param.nbEnfants;
        this.nbAdultes = param.nbAdultes;
        param.activitesChoisies.forEach(function (item) {
            _this.ajouterActivite(item);
        });
    };
    return Reservation;
}());
//# sourceMappingURL=Reservation.js.map