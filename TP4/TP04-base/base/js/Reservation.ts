﻿class Reservation {

    private arrivee: string;
    private depart: string;
    private nbAdultes: number;
    private nbEnfants: number;
    private petitDej: boolean;
    private dejeuner: boolean;
    private diner: boolean;
    private activitesChoisies: string[] = new Array();

    public get Arrivee(): string { return this.arrivee; }
    public set Arrivee(value: string) {
        this.arrivee = value;
    }

    public get Depart(): string { return this.depart; }
    public set Depart(value: string) {
        this.depart = value;
    }

    public get NbAdultes(): number { return this.nbAdultes; }
    public set NbAdultes(value: number) {
        this.nbAdultes = value;
    }

    public get NbEnfants(): number { return this.nbEnfants; }
    public set NbEnfants(value: number) {
        this.nbEnfants = value;
    }

    public get PetitDej(): boolean { return this.petitDej; }
    public set PetitDej(value: boolean) {
        this.petitDej = value;
    }

    public get Dejeuner(): boolean { return this.dejeuner; }
    public set Dejeuner(value: boolean) {
        this.dejeuner = value;
    }

    public get Diner(): boolean { return this.diner; }
    public set Diner(value: boolean) {
        this.diner = value;
    }

    public get ActivitesChoisies(): string[] { return this.activitesChoisies; }

    public ajouterActivite(s: string) {
        this.activitesChoisies.push(s);
    }

    public copier(param: any) {

        this.arrivee = param.arrivee;
        this.depart = param.depart;
        this.dejeuner = param.dejeuner;
        this.petitDej = param.petitDej;
        this.diner = param.diner;
        this.nbEnfants = param.nbEnfants;
        this.nbAdultes = param.nbAdultes;

        param.activitesChoisies.forEach((item) => {
            this.ajouterActivite(item);
        });
    }
}